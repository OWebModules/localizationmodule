﻿using LocalizationModule.Models;
using MachineLearningModule.Helpers;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocalizationModule
{
    public class CategoryController : AppController
    {
        public async Task<ResultItem<List<clsObjectRel>>> SyncCategories(SyncCategoriesRequest request)
        {
            var taskResult = await Task.Run(async() =>
            {
                var result = new ResultItem<List<clsObjectRel>>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new List<clsObjectRel>()
                };

                if (request.CancellationToken.IsCancellationRequested)
                {
                    result.ResultState = Globals.LState_Nothing.Clone();
                    result.ResultState.Additional1 = "Abbort by User!";
                    request.MessageOutput?.OutputInfo(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Get List to sync categories...");
                var syncListClasses = request.SyncList.Where(source => source.Type == Globals.Type_Class).ToList();
                var syncListObjects = request.SyncList.Where(source => source.Type == Globals.Type_Object).ToList();

                var elasticAgent = new Services.ElasticAgentCategories(Globals);

                if (syncListClasses.Any())
                {
                    var getObjectsResult = await elasticAgent.GetObjects(syncListClasses.Select(cls => new clsOntologyItem
                    {
                        GUID_Parent = cls.GUID
                    }).ToList());

                    result.ResultState = getObjectsResult.ResultState;
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    syncListObjects.AddRange(getObjectsResult.Result);
                }

                request.MessageOutput?.OutputInfo($"Have {syncListObjects.Count} items.");

                if (request.CancellationToken.IsCancellationRequested)
                {
                    result.ResultState = Globals.LState_Nothing.Clone();
                    result.ResultState.Additional1 = "Abbort by User!";
                    request.MessageOutput?.OutputInfo(result.ResultState.Additional1);
                    return result;
                }

                if (!syncListObjects.Any())
                {
                    request.MessageOutput?.OutputInfo("No objects to categorize!");
                    return result;
                }

                request.MessageOutput?.OutputInfo("Get categorized Items...");
                var searchCategorized = syncListObjects.Select(obj => new clsObjectRel
                {
                    ID_Other = obj.GUID,
                    ID_RelationType = Category.Config.LocalData.ClassRel_Category_describes.ID_RelationType,
                    ID_Parent_Object = Category.Config.LocalData.ClassRel_Category_describes.ID_Class_Left
                }).ToList();

                var categorizedSearchResult = await elasticAgent.GetRelationItems(searchCategorized);

                result.ResultState = categorizedSearchResult.ResultState;
                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError("Error while getting the categorized Elements");
                    return result;
                }

                request.MessageOutput?.OutputInfo($"Have {categorizedSearchResult.Result.Count} items.");

                if (request.CancellationToken.IsCancellationRequested)
                {
                    result.ResultState = Globals.LState_Nothing.Clone();
                    result.ResultState.Additional1 = "Abbort by User!";
                    request.MessageOutput?.OutputInfo(result.ResultState.Additional1);
                    return result;
                }

                if (!categorizedSearchResult.Result.Any())
                {
                    request.MessageOutput?.OutputInfo("No categorized objects found!");
                    return result;
                }

                var relationConfig = new clsRelationConfig(Globals);

                var categorizedList = categorizedSearchResult.Result.Select(cat => new { catRel = cat, caregory = new clsOntologyItem { GUID = cat.ID_Object, Name = cat.Name_Object, GUID_Parent = cat.ID_Parent_Object, Type = Globals.Type_Object}, lowerName = cat.Name_Other.ToLower() }).ToList();
                var unCategorizedList = (from obj in syncListObjects
                                      join categorized in categorizedList on obj.GUID equals categorized.catRel.ID_Other into categorizedList1
                                      from categorized in categorizedList1.DefaultIfEmpty()
                                      where categorized == null
                                      select new { uncategorized = obj, lowerName = obj.Name.ToLower() }).ToList();


                var categorizedItems = (from uncategorized in unCategorizedList
                                        join categoryRel in categorizedList on uncategorized.lowerName equals categoryRel.lowerName
                                        select relationConfig.Rel_ObjectRelation(categoryRel.caregory, uncategorized.uncategorized, Category.Config.LocalData.RelationType_describes))
                                        .GroupBy(rel => new {rel.ID_Object, rel.ID_Parent_Object, rel.ID_Other, rel.ID_Parent_Other, rel.ID_RelationType, rel.Ontology})
                                        .Select(relG => new clsObjectRel 
                                        { 
                                            ID_Object = relG.Key.ID_Object,
                                            ID_Parent_Object = relG.Key.ID_Parent_Object,
                                            ID_Other = relG.Key.ID_Other,
                                            ID_Parent_Other = relG.Key.ID_Parent_Other,
                                            ID_RelationType = relG.Key.ID_RelationType,
                                            OrderID = 1,
                                            Ontology = relG.Key.Ontology
                                        }).ToList();

                if (categorizedItems.Any())
                {
                    var saveResult = await elasticAgent.SaveRelations(categorizedItems);
                    result.ResultState = saveResult;
                    if (result.ResultState.GUID == Globals.LState_Error.GUID)
                    {
                        request.MessageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }
                    result.Result = categorizedItems;
                }

                return result;
            });

            return taskResult;
        }

        public CategoryController(Globals globals) : base(globals)
        {

        }
    }
}
