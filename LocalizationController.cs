﻿using LocalizationModule.Models;
using LocalizationModule.Services;
using LocalizationModule.Validation;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Attributes;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocalizationModule
{
    public class LocalizationController : AppController
    {
        public async Task<ResultItem<ImportCulturesResult>> ImportCultures(ImportCulturesRequest request)
        {
            var taskResult = await Task.Run<ResultItem<ImportCulturesResult>>(async() =>
           {
               var result = new ResultItem<ImportCulturesResult>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new ImportCulturesResult()
               };

               var elasticAgent = new ElasticAgent(Globals);

               request.MessageOutput?.OutputInfo($"Get Model...");
               var modelResult = await elasticAgent.GetImportCultureModel();
               
               result.ResultState = modelResult.ResultState;
               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo($"Have Model: {modelResult.Result.TwoLetterIsoLanguages.Count} TwoLetterIsoNames; {modelResult.Result.TwoLetterIsoLanguagesToLanguage.Count} Languagerelations"); 

               request.MessageOutput?.OutputInfo($"Get Culturinfos...");
               var cultureInfos = CultureInfo.GetCultures(CultureTypes.AllCultures);
               request.MessageOutput?.OutputInfo($"Have Culturinfos: {cultureInfos.Count()}");

               var dbCultures = (from twoLetter in modelResult.Result.TwoLetterIsoLanguages
                                 join languageToTwoLetter in modelResult.Result.TwoLetterIsoLanguagesToLanguage on twoLetter.GUID equals languageToTwoLetter.ID_Other
                                 select new { twoLetter, languageToTwoLetter });

               var cultureInfosToSave = (from cultureInfo in cultureInfos.Select(cultInfo => new { cultInfo, Name = System.Globalization.CultureInfo.CreateSpecificCulture(cultInfo.Name).Name })
                                         join imported in dbCultures on cultureInfo.Name equals imported.twoLetter.Name into dbCultures2
                                         from imported in dbCultures2.DefaultIfEmpty()
                                         where imported == null
                                         join language in modelResult.Result.Languages on cultureInfo.cultInfo.EnglishName equals language.Name into languages
                                         from language in languages.DefaultIfEmpty()
                                         select new { cultureInfo, language });

               var twoLetterIsosToSave = new List<clsOntologyItem>();
               var languagesToSave = new List<clsOntologyItem>();
               var languagesToTwoLetterIsos = new List<clsObjectRel>();
               var relationConfig = new clsRelationConfig(Globals);

               var clutureInfoNames = cultureInfosToSave.GroupBy(cultInfo => cultInfo.cultureInfo.Name).Select(cultInfoGrp => cultInfoGrp.Key).Where(cultInfoName => !string.IsNullOrEmpty(cultInfoName));

               foreach (var cultInfoName in clutureInfoNames)
               {
                   var cultureInfoToSave = cultureInfosToSave.Where(cultInfo => cultInfo.cultureInfo.Name == cultInfoName);
                   var twoLetterIso = new clsOntologyItem
                   {
                       GUID = Globals.NewGUID,
                       Name = cultInfoName,
                       GUID_Parent = Config.LocalData.Class_TwoLetterISOLanguageName.GUID,
                       Type = Globals.Type_Object
                   };
                   twoLetterIsosToSave.Add(twoLetterIso);

                   foreach (var cultInfo in cultureInfoToSave)
                   {
                       var language = cultInfo.language;
                       if (language == null)
                       {
                           language = new clsOntologyItem
                           {
                               GUID = Globals.NewGUID,
                               Name = cultInfo.cultureInfo.cultInfo.EnglishName,
                               GUID_Parent = Config.LocalData.Class_Language.GUID,
                               Type = Globals.Type_Object
                           };

                           languagesToSave.Add(language);
                       }

                       var languageRel = relationConfig.Rel_ObjectRelation(language, twoLetterIso, Config.LocalData.RelationType_is_of_Type);
                       languagesToTwoLetterIsos.Add(languageRel);
                   }
                   

                   
               }

               request.MessageOutput?.OutputInfo($"Items to save: {twoLetterIsosToSave.Count} Two Letter Isos, {languagesToSave.Count} Languages, {languagesToTwoLetterIsos.Count} Languages to Two Letter Isos");
               var saveResult = await elasticAgent.SaveTwoLetterIsos(twoLetterIsosToSave, languagesToSave, languagesToTwoLetterIsos);

               result.ResultState = saveResult;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo("Saved Two Letter Isos.");
               result.Result.CountImported = twoLetterIsosToSave.Count;

               return result;
           });

            return taskResult;
        }
        public async Task<clsOntologyItem> TranslateViewItems(object viewModel, List<GuiEntry> guiEntries)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = Globals.LState_Success.Clone();

                var properties = viewModel.GetType().GetProperties().Where(prop => prop.PropertyType == typeof(ViewItem));

                var propGuiEntries = (from prop in properties
                 join guiEntry in guiEntries on prop.Name equals guiEntry.NameGuiEntry
                 select new { ViewItem = (ViewItem)prop.GetValue(viewModel), guiEntry });

                foreach (var propGuiEntry in propGuiEntries)
                {
                    var guiCaption = propGuiEntry.guiEntry.GuiCaptions.FirstOrDefault();
                    if (guiCaption == null) continue;
                    propGuiEntry.ViewItem.ChangeViewItemValue(ViewItemType.Content.ToString(), guiCaption.NameGuiCaption);

                }

                return result;

            });

            return taskResult;
        }
        public async Task<ResultItem<GetLocalizedGuiEntriesResult>> GetLicalizedGuiEntries(GetLocalizedGuiEntriesRequest request)
        {
            var result = new ResultItem<GetLocalizedGuiEntriesResult>
            {
                ResultState = Globals.LState_Success.Clone(),
                Result = new GetLocalizedGuiEntriesResult()
            };

            var elasticAgent = new ElasticAgent(Globals);

            var serviceResult = await elasticAgent.GetLocalizationEntriesModel(request);

            result.ResultState = serviceResult.ResultState;

            if (result.ResultState.GUID == Globals.LState_Error.GUID)
            {
                return result;
            }

            result = await Task.Run<ResultItem<GetLocalizedGuiEntriesResult>>(() =>
            {
                result = new ResultItem<GetLocalizedGuiEntriesResult>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new GetLocalizedGuiEntriesResult()
                };

                result.Result.GuiEntries = (from guiEntry in serviceResult.Result.GuiEntries
                                 select new GuiEntry
                                 {
                                     IdGuiEntry = guiEntry.GUID,
                                     NameGuiEntry = guiEntry.Name
                                 }).ToList();

                foreach (var guiEntry in result.Result.GuiEntries)
                {
                    guiEntry.GuiCaptions = (from guiCaption in serviceResult.Result.GuiEntriesToGuiCaptions.Where(rel1 => rel1.ID_Object == guiEntry.IdGuiEntry)
                                       join language in serviceResult.Result.GuiCaptionsToLanguages on guiCaption.ID_Other equals language.ID_Object
                                       join twoLetterIso in serviceResult.Result.LanguagesToTwoLetterIsos.Where(rel1 => rel1.Name_Other.ToLower() == request.TwoLetterIso.ToLower()) on language.ID_Other equals twoLetterIso.ID_Object
                                       select new GuiCaption
                                       {
                                           IdGuiCaption = guiCaption.ID_Other,
                                           NameGuiCaption = guiCaption.Name_Other,
                                           Language = new Language
                                           {
                                               IdLanguage = language.ID_Other,
                                               NameLanguage = language.Name_Other,
                                               IdTwoLetterIso = twoLetterIso.ID_Other,
                                               NameTwoLetterIso = twoLetterIso.Name_Other
                                           }
                                       }).ToList();

                    guiEntry.ToolTipMessages = (from toolTipMessage in serviceResult.Result.GuiEntriesToToolTipMessages.Where(rel1 => rel1.ID_Object == guiEntry.IdGuiEntry)
                                                join language in serviceResult.Result.GuiCaptionsToLanguages on toolTipMessage.ID_Other equals language.ID_Object
                                                join twoLetterIso in serviceResult.Result.LanguagesToTwoLetterIsos.Where(rel1 => rel1.Name_Other.ToLower() == request.TwoLetterIso.ToLower()) on language.ID_Other equals twoLetterIso.ID_Object
                                                select new ToolTipMessage
                                                {
                                                    IdToolTipMessage = toolTipMessage.ID_Other,
                                                    NameToolTipMessage = toolTipMessage.Name_Other,
                                                    Language = new Language
                                                    {
                                                        IdLanguage = language.ID_Other,
                                                        NameLanguage = language.Name_Other,
                                                        IdTwoLetterIso = twoLetterIso.ID_Other,
                                                        NameTwoLetterIso = twoLetterIso.Name_Other
                                                    }
                                                }).ToList();
                }
                

                return result;
            });

            return result;
        }

        public async Task<ResultItem<List<Models.DateRange>>> GetDateRange(GetDateRangeRequest request)
        {
            var taskResult = await Task.Run(async() =>
            {
                var result = new ResultItem<List<Models.DateRange>>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new List<Models.DateRange>()
                };

                request.MessageOutput?.OutputInfo("Validate request...");

                result.ResultState = ValidationController.ValidateGetDateRangeRequest(request, Globals);

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                request.MessageOutput?.OutputInfo("Validated request.");

                request.MessageOutput?.OutputInfo("Get model...");

                var elasticAgent = new ElasticAgent(Globals);

                var serviceResult = await elasticAgent.GetDateRangeModel(request);

                result.ResultState = serviceResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    request.MessageOutput?.OutputError(result.ResultState.Additional1);
                }

                request.MessageOutput?.OutputInfo("Have model.");

                foreach (var zeitraumToRef in serviceResult.Result.ZeitraumListToReferences)
                {
                    var dateRangeAtts = serviceResult.Result.DateRanges.Where(dateR => dateR.ID_Object == zeitraumToRef.ID_Object).ToList();
                    clsObjectAtt startAtt = dateRangeAtts.Any() ? dateRangeAtts.First() : null;
                    clsObjectAtt endAtt = dateRangeAtts.Count > 1 ? dateRangeAtts[1] : null;
                    var daysBack = serviceResult.Result.DaysBack.FirstOrDefault(dateR => dateR.ID_Object == zeitraumToRef.ID_Object);
                    var hoursBack = serviceResult.Result.HoursBack.FirstOrDefault(dateR => dateR.ID_Object == zeitraumToRef.ID_Object);
                    var minutesBack = serviceResult.Result.MinutesBack.FirstOrDefault(dateR => dateR.ID_Object == zeitraumToRef.ID_Object);

                    try
                    {
                        result.Result.Add(new Models.DateRange(zeitraumToRef,
                            startAtt,
                            endAtt,
                            daysBack,
                            hoursBack,
                            minutesBack,
                            Globals));
                    }
                    catch (Exception ex)
                    {
                        result.ResultState = Globals.LState_Error.Clone();
                        result.ResultState.Additional1 = ex.Message;
                    }
                }

                return result;
            });

            return taskResult;
        }

        public LocalizationController(Globals globals) : base(globals)
        {
        }
    }
}
