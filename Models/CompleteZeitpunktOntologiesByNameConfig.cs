﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocalizationModule.Models
{
    public class CompleteZeitpunktOntologiesByNameConfig
    {
        public clsOntologyItem RootConfig { get; set; }
        public List<clsOntologyItem> Configs { get; set; } = new List<clsOntologyItem>();
        public List<clsObjectRel> ConfigsToZeitpunkte { get; set; } = new List<clsObjectRel>();

        public bool AllZeitpunkte
        {
            get
            {
                var result = !Configs.Any();
                if (result) return result;

                result = (from config in Configs
                          join configToZeitpunkt in ConfigsToZeitpunkte on config.GUID equals configToZeitpunkt.ID_Object into configsToZeitpunkte
                          from configToZeitpunkt in configsToZeitpunkte.DefaultIfEmpty()
                          where configToZeitpunkt == null
                          select config).Any();

                 return result;
            }
        }
    }
}
