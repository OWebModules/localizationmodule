﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocalizationModule.Models
{
    public class CompleteZeitpunktControllerZeitpunktListMap
    {
        public ZeitpunktControllerBase Controller { get; set; }
        public List<clsOntologyItem> ZeitpunktListToComplete { get; set; } = new List<clsOntologyItem>();
    }
}
