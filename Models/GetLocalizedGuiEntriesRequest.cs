﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocalizationModule.Models
{
    public class GetLocalizedGuiEntriesRequest
    {
        public string IdReference { get; private set; }
        public string TwoLetterIso { get; private set; }

        public GetLocalizedGuiEntriesRequest(string idReference, string twoLetterIso)
        {
            IdReference = idReference;
            TwoLetterIso = twoLetterIso;
        }
    }
}
