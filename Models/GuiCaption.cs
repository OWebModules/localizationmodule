﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocalizationModule.Models
{
    public class GuiCaption
    {
        public string IdGuiCaption { get; set; }
        public string NameGuiCaption { get; set; }
        public Language Language { get; set; }

    }
}
