﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocalizationModule.Models
{
    public class GetDateRangeRequest
    {
        public List<string> IdReferences { get; private set; } = new List<string>();
        public IMessageOutput MessageOutput { get; set; }

        public GetDateRangeRequest(List<string> idReferences)
        {
            IdReferences = idReferences;
        }
    }
}
