﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocalizationModule.Models
{
    public class DateRange
    {
        public clsOntologyItem Zeitraum { get; private set; }

        public clsOntologyItem Reference { get; private set; }

        public clsObjectAtt StartAtt { get; private set; }

        public clsObjectAtt EndAtt { get; private set; }

        public clsObjectAtt DaysBack { get; private set; }

        public clsObjectAtt HoursBack { get; private set; }

        public clsObjectAtt MinutesBack { get; private set; }

        public DateTime Start { get; set; }
        public DateTime End { get; set; }

        public DateRange(clsObjectRel zeitRaumToReference, 
            clsObjectAtt startAtt,
            clsObjectAtt endAtt,
            clsObjectAtt daysBack,
            clsObjectAtt hoursBack,
            clsObjectAtt minutesBack,
            Globals globals)
        {
            Zeitraum = new clsOntologyItem
            {
                GUID = zeitRaumToReference.ID_Object,
                Name = zeitRaumToReference.Name_Object,
                GUID_Parent = zeitRaumToReference.ID_Parent_Object,
                Type = globals.Type_Object,
            };

            Reference = new clsOntologyItem
            {
                GUID = zeitRaumToReference.ID_Other,
                Name = zeitRaumToReference.Name_Other,
                GUID_Parent = zeitRaumToReference.ID_Parent_Other,
                Type = zeitRaumToReference.Ontology
            };

            StartAtt = startAtt;
            EndAtt = endAtt;
            DaysBack = daysBack;
            HoursBack = hoursBack;
            MinutesBack = minutesBack;

            SetStartEnd();
        }

        private void SetStartEnd()
        {
            if (StartAtt != null)
            {
                Start = StartAtt.Val_Datetime.Value;
                if (EndAtt != null)
                {
                    End = EndAtt.Val_Datetime.Value;
                }
                else
                {
                    End = DateTime.Now;
                }
                return;
            }
            
            if (StartAtt == null && EndAtt != null)
            {
                Start = DateTime.MinValue;
                return;
            }

            End = DateTime.Now;
            var start = End;
            var substracted = false;
            if (DaysBack != null)
            {
                start = start.AddDays(-1 * DaysBack.Val_Int.Value);
                substracted = true;
            }
            if (HoursBack != null)
            {
                start = start.AddHours(-1 * HoursBack.Val_Int.Value);
                substracted = true;
            }
            if (MinutesBack != null)
            {
                start = start.AddMinutes (-1 * MinutesBack.Val_Int.Value);
                substracted = true;
            }
            Start = start;

            if (substracted) return;

            throw new Exception("You have define at least one attribute!");


        }
    }
}
