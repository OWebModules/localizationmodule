﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LocalizationModule.Models
{
    public class ZeitpunktModel
    { 
        public Regex RegexCentury { get; private set; }
        public Regex RegexYear { get; private set; }
        public Regex RegexMonth { get; private set; }
        public Regex RegexDay { get; private set; }
        public Regex RegexHour { get; private set; }
        public Regex RegexMinute { get; private set; }

        public Regex RegexSecond { get; private set; }

        public Regex RegexMillisecond { get; private set; }
        public Regex RegexMicroSecond { get; private set; }

        public Regex RegexNanoSecond { get; private set; }

        public Regex RegexPikoSecond { get; private set; }

        public Regex RegexWeek { get; private set; }

        public clsOntologyItem Zeitpunkt {get; set;}
        public clsOntologyItem Century { get; set; }
        public clsOntologyItem Day { get; set; }
        public clsOntologyItem Hour { get; set; }
        public clsOntologyItem Microsecond { get; set; }
        public clsOntologyItem Millisecond { get; set; }
        public clsOntologyItem Minute { get; set; }
        public clsOntologyItem Month { get; set; }
        public clsOntologyItem Nanosecond { get; set; }
        public clsOntologyItem Picosecond { get; set; }
        public clsOntologyItem Second { get; set; }
        public clsOntologyItem Week { get; set; }
        public clsOntologyItem Weekdays { get; set; }
        public clsOntologyItem Year { get; set; }
        
        public bool IsValid { get; private set; }
        public string ValidationMessage { get; private set; }

        public DateTime? ZeitpunktDatetime { get; private set; }

        public ZeitpunktModel()
        {
            Initialize();
        }

        private void Initialize()
        {
            RegexCentury = new Regex(@"\d+C");
            RegexYear = new Regex(@"(-)?\d+Y");
            RegexMonth = new Regex(@"\d+M");
            RegexWeek = new Regex(@"\d+W");
            RegexDay = new Regex(@"\d+D");
            RegexHour = new Regex(@"\d+h");
            RegexMinute = new Regex(@"\d+m");
            RegexSecond = new Regex(@"\d+s");
            RegexMillisecond = new Regex(@"\d+ms");
            RegexMicroSecond = new Regex(@"\d+µs");
            RegexNanoSecond = new Regex(@"\d+ns");
            RegexPikoSecond = new Regex(@"\d+ps");
        }

        public ZeitpunktModel(string name)
        {
            Initialize();
            DateTime dateTime;
            if (DateTime.TryParse(name, out dateTime))
            {
                Year = new clsOntologyItem
                {
                    Name = dateTime.Year.ToString(),
                    GUID_Parent = Config.LocalData.Class_Year.GUID
                };

                Month = new clsOntologyItem
                {
                    Name = dateTime.Month.ToString(),
                    GUID_Parent = Config.LocalData.Class_Month.GUID
                };

                Day = new clsOntologyItem
                {
                    Name = dateTime.Day.ToString(),
                    GUID_Parent = Config.LocalData.Class_Day.GUID
                };

                Hour = new clsOntologyItem
                {
                    Name = dateTime.Hour.ToString(),
                    GUID_Parent = Config.LocalData.Class_Hour.GUID
                };

                Minute = new clsOntologyItem
                {
                    Name = dateTime.Minute.ToString(),
                    GUID_Parent = Config.LocalData.Class_Minute.GUID
                };

                Second = new clsOntologyItem
                {
                    Name = dateTime.Second.ToString(),
                    GUID_Parent = Config.LocalData.Class_Second.GUID
                };

                Millisecond = new clsOntologyItem
                {
                    Name = dateTime.Millisecond.ToString(),
                    GUID_Parent = Config.LocalData.Class_Millisecond.GUID
                };

            }
            else
            {
                

                var centuryMatch = RegexCentury.Match(name);
                if (centuryMatch.Success)
                {
                    var centuryValue = GetNumberValue(centuryMatch.Value);
                    if (centuryValue != null && centuryValue >= 0)
                    {
                        Century = new clsOntologyItem
                        {
                            Name = centuryValue.ToString(),
                            GUID_Parent = Config.LocalData.Class_Century.GUID
                        };
                    }
                    
                }

                var yearMatch = RegexYear.Match(name);
                if (yearMatch.Success)
                {
                    var yearValue = GetNumberValue(yearMatch.Value);
                    if (yearValue != null)
                    {
                        Year = new clsOntologyItem
                        {
                            Name = yearValue.ToString(),
                            GUID_Parent = Config.LocalData.Class_Year.GUID
                        };
                    }
                }

                var monthMatch = RegexMonth.Match(name);
                if (monthMatch.Success)
                {
                    var monthValue = GetNumberValue(monthMatch.Value);
                    if (monthValue != null && monthValue.Value > 0 && monthValue.Value < 13)
                    {
                        Month = new clsOntologyItem
                        {
                            Name = monthValue.ToString(),
                            GUID_Parent = Config.LocalData.Class_Month.GUID
                        };
                    }
                }

                var weekMatch = RegexWeek.Match(name);
                if (weekMatch.Success)
                {
                    var weekValue = GetNumberValue(weekMatch.Value);
                    if (weekValue != null && weekValue.Value > 0 && weekValue.Value < 53)
                    {
                        Week = new clsOntologyItem
                        {
                            Name = weekValue.ToString(),
                            GUID_Parent = Config.LocalData.Class_Week.GUID
                        };
                    }
                }

                var dayMatch = RegexDay.Match(name);
                if (dayMatch.Success)
                {
                    var dayValue = GetNumberValue(dayMatch.Value);
                    if (dayValue != null && dayValue > 0 && dayValue < 31)
                    {

                        Day = new clsOntologyItem
                        {
                            Name = dayValue.ToString(),
                            GUID_Parent = Config.LocalData.Class_Day.GUID
                        };
                    }
                }

                var hourMatch = RegexHour.Match(name);

                if (hourMatch.Success)
                {
                    var hourValue = GetNumberValue(hourMatch.Value);
                    if (hourValue != null && hourValue >= 0 && hourValue <= 23)
                    {
                        Hour = new clsOntologyItem
                        {
                            Name = hourValue.ToString(),
                            GUID_Parent = Config.LocalData.Class_Hour.GUID
                        };
                    }
                }

                var minuteMatch = RegexMinute.Match(name);

                if (minuteMatch.Success)
                {
                    var minuteValue = GetNumberValue(minuteMatch.Value);
                    if (minuteValue != null && minuteValue >= 0 && minuteValue <= 60)
                    {
                        Minute = new clsOntologyItem
                        {
                            Name = minuteValue.ToString(),
                            GUID_Parent = Config.LocalData.Class_Minute.GUID
                        };
                    }
                }

                var secondMatch = RegexSecond.Match(name);

                if (secondMatch.Success)
                {
                    var secondValue = GetNumberValue(secondMatch.Value);
                    if (secondValue != null && secondValue >= 0 && secondValue <= 60)
                    {
                        Second = new clsOntologyItem
                        {
                            Name = secondValue.ToString(),
                            GUID_Parent = Config.LocalData.Class_Second.GUID
                        };
                    }
                }

                var millisecondMatch = RegexMillisecond.Match(name);

                if (millisecondMatch.Success)
                {
                    var millisecondValue = GetNumberValue(millisecondMatch.Value);
                    if (millisecondValue != null && millisecondValue >= 0 && millisecondValue <= 1000)
                    {
                        Millisecond = new clsOntologyItem
                        {
                            Name = millisecondValue.ToString(),
                            GUID_Parent = Config.LocalData.Class_Millisecond.GUID
                        };
                    }
                }

                var microsecondMatch = RegexMicroSecond.Match(name);

                if (microsecondMatch.Success)
                {
                    var microsecondValue = GetNumberValue(microsecondMatch.Value);
                    if (microsecondValue != null && microsecondValue >= 0 && microsecondValue <= 1000)
                    {
                        Microsecond = new clsOntologyItem
                        {
                            Name = microsecondValue.ToString(),
                            GUID_Parent = Config.LocalData.Class_Microsecond.GUID
                        };
                    }
                }

                var nanosecondMatch = RegexNanoSecond.Match(name);

                if (nanosecondMatch.Success)
                {
                    var nanosecondValue = GetNumberValue(nanosecondMatch.Value);
                    if (nanosecondValue != null && nanosecondValue >= 0 && nanosecondValue <= 1000)
                    {
                        Nanosecond = new clsOntologyItem
                        {
                            Name = nanosecondValue.ToString(),
                            GUID_Parent = Config.LocalData.Class_Nanosecond.GUID
                        };
                    }
                }

                var pikosecondMatch = RegexPikoSecond.Match(name);

                if (pikosecondMatch.Success)
                {
                    var pikosecondValue = GetNumberValue(pikosecondMatch.Value);
                    if (pikosecondValue != null && pikosecondValue >= 0 && pikosecondValue <= 1000)
                    {
                        Picosecond = new clsOntologyItem
                        {
                            Name = pikosecondValue.ToString(),
                            GUID_Parent = Config.LocalData.Class_Picosecond.GUID
                        };
                    }
                }
            }
        }

        private long? GetNumberValue(string name)
        {
            var regexSignedNumber = new Regex(@"(-)?\d+");
            var numberMatch = regexSignedNumber.Match(name);

            if (!numberMatch.Success)
            {
                return null;
            }

            long longValue;
            if (!long.TryParse(numberMatch.Value, out longValue))
            {
                return null;
            }
            return longValue;

        }


        public bool CreateDatetimeZeitpunkt()
        { 

                
                if (Year != null && Month != null && Day != null)
                {
                    int year;
                    int month;
                    int day;

                    int hour = 0;
                    int minute = 0;
                    int second = 0;
                    int millisecond = 0;

                    if (!int.TryParse(Year.Name, out year))
                    {
                        IsValid = false;
                        ValidationMessage = "No valid year!";
                        return IsValid;
                    }
                    
                    if (!int.TryParse(Month.Name, out month))
                    {
                        IsValid = false;
                        ValidationMessage = "No valid month!";
                        return IsValid;
                    }

                    if (!int.TryParse(Day.Name, out day))
                    {
                        IsValid = false;
                        ValidationMessage = "No valid day!";
                        return IsValid;
                    }

                    if (Hour != null)
                    {
                        if (!int.TryParse(Hour.Name, out hour))
                        {
                            IsValid = false;
                            ValidationMessage = "No valid hour!";
                            return IsValid;
                        }
                    }

                    if (Minute != null)
                    {
                        if (!int.TryParse(Minute.Name, out minute))
                        {
                            IsValid = false;
                            ValidationMessage = "No valid minute!";
                            return IsValid;
                        }
                    }

                    if (Second != null)
                    {
                        if (!int.TryParse(Second.Name, out second))
                        {
                            IsValid = false;
                            ValidationMessage = "No valid second!";
                            return IsValid;
                        }
                    }

                    if (Millisecond != null)
                    {
                        if (!int.TryParse(Millisecond.Name, out millisecond))
                        {
                            IsValid = false;
                            ValidationMessage = "No valid millisecond!";
                            return IsValid;
                        }
                    }

                    ZeitpunktDatetime = new DateTime(year, month, day, hour, minute, second, millisecond);
                }

            return IsValid;      
            
        }

        
    }
}
