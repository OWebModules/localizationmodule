﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocalizationModule.Models
{
    public abstract class ZeitpunktControllerBase
    {
        protected Globals globals;
        public List<clsOntologyItem> ZeitpunktList { get; private set; } = new List<clsOntologyItem>();
        public virtual bool IsResponsible(string name)
        {
            throw new NotImplementedException();
        }

        public virtual async Task<ResultItem<CompleteZeitpunktOntologiesByNamesResult>> CompleteZeitpunkte(List<clsOntologyItem> zeitpunktItemList, IMessageOutput messageOutput)
        {
            throw new NotImplementedException();
        }

        public ZeitpunktControllerBase(Globals globals)
        {
            this.globals = globals;
        }
    }
}
