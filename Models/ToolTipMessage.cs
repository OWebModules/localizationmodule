﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocalizationModule.Models
{
    public class ToolTipMessage
    {
        public string IdToolTipMessage { get; set; }
        public string NameToolTipMessage { get; set; }
        public Language Language { get; set; }
        
    }
}
