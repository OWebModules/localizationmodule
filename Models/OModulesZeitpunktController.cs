﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LocalizationModule.Models
{
    public class OModulesZeitpunktController : ZeitpunktControllerBase
    {

        private Regex regexCentury = new Regex(@"(-)?\d+C");
        private Regex regexYear = new Regex(@"(-)?\d+Y");
        private Regex regexMonth = new Regex(@"(-)?\d+M");
        private Regex regexDay = new Regex(@"(-)?\d+D");
        private Regex regexWeek = new Regex(@"(-)?\d+W");
        private Regex regexHour = new Regex(@"(-)?\d+h");
        private Regex regexMinute = new Regex(@"(-)?\d+m");
        private Regex regexSecond = new Regex(@"(-)?\d+s");
        private Regex regexMillisecond = new Regex(@"(-)?\d+ms");
        private Regex regexMicrosecond = new Regex(@"(-)?\d+µs");
        private Regex regexNanosecond = new Regex(@"(-)?\d+ns");
        private Regex regexPicosecond = new Regex(@"(-)?\d+ps");


        public override bool IsResponsible(string name)
        {
            return (regexCentury.Match(name).Success ||
                regexYear.Match(name).Success ||
                regexMonth.Match(name).Success ||
                regexDay.Match(name).Success ||
                regexWeek.Match(name).Success ||
                regexHour.Match(name).Success ||
                regexMinute.Match(name).Success ||
                regexSecond.Match(name).Success ||
                regexMillisecond.Match(name).Success ||
                regexMicrosecond.Match(name).Success ||
                regexNanosecond.Match(name).Success ||
                regexPicosecond.Match(name).Success);
        }

        public override async Task<ResultItem<CompleteZeitpunktOntologiesByNamesResult>> CompleteZeitpunkte(List<clsOntologyItem> zeitpunktItemList, IMessageOutput messageOutput)
        {
            var taskResult = await Task.Run<ResultItem<CompleteZeitpunktOntologiesByNamesResult>>(async () =>
           {
               var result = new ResultItem<CompleteZeitpunktOntologiesByNamesResult>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new CompleteZeitpunktOntologiesByNamesResult()
               };

               foreach (var zeitpunkt in zeitpunktItemList)
               {
                   zeitpunkt.OList_Rel = new List<clsOntologyItem>();
                   var centuryMatch = regexCentury.Match(zeitpunkt.Name);
                   if (centuryMatch.Success)
                   {
                       var centuryValue = GetNumberValue(centuryMatch.Value);
                       if (centuryValue != null && centuryValue >= 0)
                       {
                           zeitpunkt.OList_Rel.Add(new clsOntologyItem
                           {
                               Name = centuryValue.ToString(),
                               GUID_Parent = Config.LocalData.Class_Century.GUID,
                               Type = globals.Type_Object
                           });
                       }

                   }

                   var yearMatch = regexYear.Match(zeitpunkt.Name);
                   if (yearMatch.Success)
                   {
                       var yearValue = GetNumberValue(yearMatch.Value);
                       if (yearValue != null)
                       {
                           zeitpunkt.OList_Rel.Add(new clsOntologyItem
                           {
                               Name = yearValue.ToString(),
                               GUID_Parent = Config.LocalData.Class_Year.GUID,
                               Type = globals.Type_Object
                           });
                       }
                   }

                   var monthMatch = regexMonth.Match(zeitpunkt.Name);
                   if (monthMatch.Success)
                   {
                       var monthValue = GetNumberValue(monthMatch.Value);
                       if (monthValue != null && monthValue.Value > 0 && monthValue.Value < 13)
                       {
                           zeitpunkt.OList_Rel.Add(new clsOntologyItem
                           {
                               Name = monthValue.ToString(),
                               GUID_Parent = Config.LocalData.Class_Month.GUID,
                               Type = globals.Type_Object
                           });
                       }
                   }

                   var weekMatch = regexWeek.Match(zeitpunkt.Name);
                   if (weekMatch.Success)
                   {
                       var weekValue = GetNumberValue(weekMatch.Value);
                       if (weekValue != null && weekValue.Value > 0 && weekValue.Value < 53)
                       {
                           zeitpunkt.OList_Rel.Add(new clsOntologyItem
                           {
                               Name = weekValue.ToString(),
                               GUID_Parent = Config.LocalData.Class_Week.GUID,
                               Type = globals.Type_Object
                           });
                       }
                   }

                   var dayMatch = regexDay.Match(zeitpunkt.Name);
                   if (dayMatch.Success)
                   {
                       var dayValue = GetNumberValue(dayMatch.Value);
                       if (dayValue != null && dayValue > 0 && dayValue < 31)
                       {

                           zeitpunkt.OList_Rel.Add(new clsOntologyItem
                           {
                               Name = dayValue.ToString(),
                               GUID_Parent = Config.LocalData.Class_Day.GUID,
                               Type = globals.Type_Object
                           });
                       }
                   }

                   var hourMatch = regexHour.Match(zeitpunkt.Name);

                   if (hourMatch.Success)
                   {
                       var hourValue = GetNumberValue(hourMatch.Value);
                       if (hourValue != null && hourValue >= 0 && hourValue <= 23)
                       {
                           zeitpunkt.OList_Rel.Add(new clsOntologyItem
                           {
                               Name = hourValue.ToString(),
                               GUID_Parent = Config.LocalData.Class_Hour.GUID,
                               Type = globals.Type_Object
                           });
                       }
                   }

                   var minuteMatch = regexMinute.Match(zeitpunkt.Name);

                   if (minuteMatch.Success)
                   {
                       var minuteValue = GetNumberValue(minuteMatch.Value);
                       if (minuteValue != null && minuteValue >= 0 && minuteValue <= 60)
                       {
                           zeitpunkt.OList_Rel.Add(new clsOntologyItem
                           {
                               Name = minuteValue.ToString(),
                               GUID_Parent = Config.LocalData.Class_Minute.GUID,
                               Type = globals.Type_Object
                           });
                       }
                   }

                   var secondMatch = regexSecond.Match(zeitpunkt.Name);

                   if (secondMatch.Success)
                   {
                       var secondValue = GetNumberValue(secondMatch.Value);
                       if (secondValue != null && secondValue >= 0 && secondValue <= 60)
                       {
                           zeitpunkt.OList_Rel.Add(new clsOntologyItem
                           {
                               Name = secondValue.ToString(),
                               GUID_Parent = Config.LocalData.Class_Second.GUID,
                               Type = globals.Type_Object
                           });
                       }
                   }

                   var millisecondMatch = regexMillisecond.Match(zeitpunkt.Name);

                   if (millisecondMatch.Success)
                   {
                       var millisecondValue = GetNumberValue(millisecondMatch.Value);
                       if (millisecondValue != null && millisecondValue >= 0 && millisecondValue <= 1000)
                       {
                           zeitpunkt.OList_Rel.Add(new clsOntologyItem
                           {
                               Name = millisecondValue.ToString(),
                               GUID_Parent = Config.LocalData.Class_Millisecond.GUID,
                               Type = globals.Type_Object
                           });
                       }
                   }

                   var microsecondMatch = regexMicrosecond.Match(zeitpunkt.Name);

                   if (microsecondMatch.Success)
                   {
                       var microsecondValue = GetNumberValue(microsecondMatch.Value);
                       if (microsecondValue != null && microsecondValue >= 0 && microsecondValue <= 1000)
                       {
                           zeitpunkt.OList_Rel.Add(new clsOntologyItem
                           {
                               Name = microsecondValue.ToString(),
                               GUID_Parent = Config.LocalData.Class_Microsecond.GUID,
                               Type = globals.Type_Object
                           });
                       }
                   }

                   var nanosecondMatch = regexNanosecond.Match(zeitpunkt.Name);

                   if (nanosecondMatch.Success)
                   {
                       var nanosecondValue = GetNumberValue(nanosecondMatch.Value);
                       if (nanosecondValue != null && nanosecondValue >= 0 && nanosecondValue <= 1000)
                       {
                           zeitpunkt.OList_Rel.Add(new clsOntologyItem
                           {
                               Name = nanosecondValue.ToString(),
                               GUID_Parent = Config.LocalData.Class_Nanosecond.GUID,
                               Type = globals.Type_Object
                           });
                       }
                   }

                   var pikosecondMatch = regexPicosecond.Match(zeitpunkt.Name);

                   if (pikosecondMatch.Success)
                   {
                       var pikosecondValue = GetNumberValue(pikosecondMatch.Value);
                       if (pikosecondValue != null && pikosecondValue >= 0 && pikosecondValue <= 1000)
                       {
                           zeitpunkt.OList_Rel.Add(new clsOntologyItem
                           {
                               Name = pikosecondValue.ToString(),
                               GUID_Parent = Config.LocalData.Class_Picosecond.GUID,
                               Type = globals.Type_Object
                           });
                       }
                   }
               }

               var dbConnector = new OntologyModDBConnector(globals);

               var zeitpunktItems = zeitpunktItemList.SelectMany(zeitP => zeitP.OList_Rel);
               var searchObjects = zeitpunktItemList
                    .SelectMany(zeitpunkt => zeitpunkt.OList_Rel)
                    .GroupBy(zeitpunktItem => new { zeitpunktItem.Name, zeitpunktItem.GUID_Parent })
                    .Select(grp => new clsOntologyItem { Name = grp.Key.Name, GUID_Parent = grp.Key.GUID_Parent }).ToList();

               if (searchObjects.Any())
               {
                   result.ResultState = dbConnector.GetDataObjects(searchObjects);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the objects!";
                       return result;
                   }

                   (from zeitpunktItem in zeitpunktItems
                    join dbItem in dbConnector.Objects1 on new { zeitpunktItem.Name, zeitpunktItem.GUID_Parent } equals
                                                           new { dbItem.Name, dbItem.GUID_Parent }
                    select new { zeitpunktItem, dbItem }).ToList().ForEach(item =>
                    {
                        item.zeitpunktItem.GUID = item.dbItem.GUID;
                        item.zeitpunktItem.New_Item = false;
                    });
               }

               var itemsToSave = zeitpunktItems
                        .Where(zeitPItem => zeitPItem.New_Item == null)
                        .GroupBy(zeitPItem => new { zeitPItem.Name, zeitPItem.GUID_Parent })
                        .Select(grp => new clsOntologyItem
                        {
                            GUID = globals.NewGUID,
                            Name = grp.Key.Name,
                            GUID_Parent = grp.Key.GUID_Parent,
                            Type = globals.Type_Object
                        }).ToList();

               if (itemsToSave.Any())
               {
                   result.ResultState = dbConnector.SaveObjects(itemsToSave);
                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while save the objects!";
                       return result;
                   }
               }

               (from zeitPunktItem in zeitpunktItems
                join savedItem in itemsToSave on new { zeitPunktItem.Name, zeitPunktItem.GUID_Parent } equals
                                                 new { savedItem.Name, savedItem.GUID_Parent }
                select new { zeitPunktItem, savedItem }).ToList().ForEach(zeitPItem =>
                {
                    zeitPItem.zeitPunktItem.GUID = zeitPItem.savedItem.GUID;
                    zeitPItem.zeitPunktItem.Type = zeitPItem.savedItem.Type;
                });

               var checkRelations = new List<clsObjectRel>();
               var relationConfig = new clsRelationConfig(globals);
               foreach (var zeitpunkt in zeitpunktItemList)
               {
                   checkRelations.AddRange(zeitpunkt.OList_Rel.Select(rel => relationConfig.Rel_ObjectRelation(zeitpunkt, rel, Config.LocalData.RelationType_belonging)));
               }

               var searchRelations = checkRelations
                    .GroupBy(rel => new { rel.ID_Object, rel.ID_RelationType, rel.ID_Other })
                    .Select(grp => new clsObjectRel
                    {
                        ID_Object = grp.Key.ID_Object,
                        ID_RelationType = Config.LocalData.RelationType_belonging.GUID,
                        ID_Other = grp.Key.ID_Other
                    }).ToList();

               if (searchRelations.Any())
               {
                   result.ResultState = dbConnector.GetDataObjectRel(searchRelations);
                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the relations!";
                       return result;
                   }
               }

               var saveRelations =(from checkRelation in checkRelations
                join dbRel in dbConnector.ObjectRels on new { checkRelation.ID_Object, checkRelation.ID_RelationType, checkRelation.ID_Other } equals
                                                        new { dbRel.ID_Object, dbRel.ID_RelationType, dbRel.ID_Other } into dbRels
                from dbRel in dbRels.DefaultIfEmpty()
                where dbRel == null
                select checkRelation).ToList();


               if (saveRelations.Any())
               {
                   result.ResultState = dbConnector.SaveObjRel(saveRelations);
                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the relations!";
                       return result;
                   }
               }

               result.Result.SucceededCompletes = zeitpunktItemList;

               return result;
           });

            return taskResult;
        }

        private long? GetNumberValue(string name)
        {
            var regexSignedNumber = new Regex(@"(-)?\d+");
            var numberMatch = regexSignedNumber.Match(name);

            if (!numberMatch.Success)
            {
                return null;
            }

            long longValue;
            if (!long.TryParse(numberMatch.Value, out longValue))
            {
                return null;
            }
            return longValue;

        }

        public OModulesZeitpunktController(Globals globals) : base(globals)
        {

        }
    }
}
