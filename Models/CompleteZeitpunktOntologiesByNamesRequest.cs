﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocalizationModule.Models
{
    public class CompleteZeitpunktOntologiesByNamesRequest
    {
        public string IdConfig { get; set; }
        public IMessageOutput MessageOutput { get; set; }
    }
}
