﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocalizationModule.Models
{
    public class ImportCulturesModel
    {
        public List<clsOntologyItem> TwoLetterIsoLanguages { get; set; } = new List<clsOntologyItem>();
        public List<clsObjectRel> TwoLetterIsoLanguagesToLanguage { get; set; } = new List<clsObjectRel>();
        public List<clsOntologyItem> Languages { get; set; } = new List<clsOntologyItem>();
    }
}
