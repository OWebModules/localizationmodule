﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocalizationModule.Models
{
    public class GuiEntry
    {
        public string IdGuiEntry { get; set; }
        public string NameGuiEntry { get; set; }

        public List<GuiCaption> GuiCaptions { get; set; } = new List<GuiCaption>();
        public List<ToolTipMessage> ToolTipMessages { get; set; } = new List<ToolTipMessage>();
    }
}
