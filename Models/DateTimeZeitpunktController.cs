﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LocalizationModule.Models
{
    public class DateTimeZeitpunktController : ZeitpunktControllerBase
    {
        private Regex regexTime = new Regex(@"^(\d{2}:\d{2}:\d{2}|\d{2}:\d{2}|\d{2}:\d{2}:\d{2}\.\d{3})$");
        public override bool IsResponsible(string name)
        {
            DateTime dateTime;
            var result = DateTime.TryParse(name, out dateTime);

            if (!result)
            {
                return false;
            }

            return true;
        }

        public override async Task<ResultItem<CompleteZeitpunktOntologiesByNamesResult>> CompleteZeitpunkte(List<clsOntologyItem> zeitpunktItemList, IMessageOutput messageOutput)
        {
            var taskResult = await Task.Run<ResultItem<CompleteZeitpunktOntologiesByNamesResult>>(async() =>
            {
                var count = 100;
                var pos = 0;

                if (pos + count > zeitpunktItemList.Count)
                {
                    count = zeitpunktItemList.Count - pos;
                }

                var result = new ResultItem<CompleteZeitpunktOntologiesByNamesResult>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new CompleteZeitpunktOntologiesByNamesResult()
                };

                while (pos + count <= zeitpunktItemList.Count && count > 0)
                {
                    result = await CompleteZeitpunktLoc(zeitpunktItemList.GetRange(pos, count), messageOutput);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        return result;
                    }
                    pos += count;
                    if (pos + count > zeitpunktItemList.Count)
                    {
                        count = zeitpunktItemList.Count - pos;
                    }
                }
                
                return result;
            });
            return taskResult;
        }

        private async Task<ResultItem<CompleteZeitpunktOntologiesByNamesResult>> CompleteZeitpunktLoc(List<clsOntologyItem> zeitpunktItemList, IMessageOutput messageOutput)
        {
            var taskResult = await Task.Run<ResultItem<CompleteZeitpunktOntologiesByNamesResult>>(() =>
            {
                var result = new ResultItem<CompleteZeitpunktOntologiesByNamesResult>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new CompleteZeitpunktOntologiesByNamesResult()
                };

                messageOutput?.OutputInfo($"Get Objects to check of {zeitpunktItemList.Count} Zeitpunkte ...");
                var objectsToCheck = new List<clsOntologyItem>();
                var relationsToCheck = new List<clsObjectRel>();
                var relationConfig = new clsRelationConfig(globals);
                foreach (var zeitpunktItem in zeitpunktItemList)
                {
                    if (regexTime.Match(zeitpunktItem.Name).Success)
                    {
                        zeitpunktItem.Mark = false;   
                    }
                    else
                    {
                        zeitpunktItem.Mark = true;
                    }
                    zeitpunktItem.OList_Rel = new List<clsOntologyItem>();
                    var zeitpunkt = DateTime.Parse(zeitpunktItem.Name);
                    if (zeitpunktItem.Mark.Value)
                    {
                        zeitpunktItem.Val_Date = zeitpunkt;
                        var itemYear = new clsOntologyItem
                        {
                            Name = zeitpunkt.Year.ToString(),
                            GUID_Parent = Config.LocalData.Class_Year.GUID,
                            Type = globals.Type_Object,
                            GUID_Related = zeitpunktItem.GUID
                        };

                        zeitpunktItem.OList_Rel.Add(itemYear);
                        objectsToCheck.Add(itemYear);

                        var itemMonth = new clsOntologyItem
                        {
                            Name = zeitpunkt.Month.ToString(),
                            GUID_Parent = Config.LocalData.Class_Month.GUID,
                            Type = globals.Type_Object,
                            GUID_Related = zeitpunktItem.GUID
                        };
                        zeitpunktItem.OList_Rel.Add(itemMonth);
                        objectsToCheck.Add(itemMonth);

                        var itemDay = new clsOntologyItem
                        {
                            Name = zeitpunkt.Day.ToString(),
                            GUID_Parent = Config.LocalData.Class_Day.GUID,
                            Type = globals.Type_Object,
                            GUID_Related = zeitpunktItem.GUID
                        };
                        zeitpunktItem.OList_Rel.Add(itemDay);
                        objectsToCheck.Add(itemDay);
                    }
                    
                    var itemHour = new clsOntologyItem
                    {
                        Name = zeitpunkt.Hour.ToString(),
                        GUID_Parent = Config.LocalData.Class_Hour.GUID,
                        Type = globals.Type_Object,
                        GUID_Related = zeitpunktItem.GUID
                    };
                    zeitpunktItem.OList_Rel.Add(itemHour);
                    objectsToCheck.Add(itemHour);

                    var itemMinute = new clsOntologyItem
                    {
                        Name = zeitpunkt.Minute.ToString(),
                        GUID_Parent = Config.LocalData.Class_Minute.GUID,
                        Type = globals.Type_Object,
                        GUID_Related = zeitpunktItem.GUID
                    };
                    zeitpunktItem.OList_Rel.Add(itemMinute);
                    objectsToCheck.Add(itemMinute);

                    var itemSecond = new clsOntologyItem
                    {
                        Name = zeitpunkt.Second.ToString(),
                        GUID_Parent = Config.LocalData.Class_Second.GUID,
                        Type = globals.Type_Object,
                        GUID_Related = zeitpunktItem.GUID
                    };
                    zeitpunktItem.OList_Rel.Add(itemSecond);
                    objectsToCheck.Add(itemSecond);

                    var itemMilliSecond = new clsOntologyItem
                    {
                        Name = zeitpunkt.Millisecond.ToString(),
                        GUID_Parent = Config.LocalData.Class_Millisecond.GUID,
                        Type = globals.Type_Object,
                        GUID_Related = zeitpunktItem.GUID
                    };
                    zeitpunktItem.OList_Rel.Add(itemMilliSecond);
                    objectsToCheck.Add(itemMilliSecond);
                }

                var objectsToQuery = objectsToCheck
                     .GroupBy(zeitUnit => new { Name = zeitUnit.Name, GUID_Parent = zeitUnit.GUID_Parent })
                     .Select(grp => new clsOntologyItem { Name = grp.Key.Name, GUID_Parent = grp.Key.GUID_Parent }).ToList();

                messageOutput?.OutputInfo($"Have {objectsToQuery.Count} objects to check.");

                var dbConnector = new OntologyModDBConnector(globals);

                messageOutput?.OutputInfo($"Query objects...");
                result.ResultState = dbConnector.GetDataObjects(objectsToQuery);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the related objects!";
                    messageOutput?.OutputError(result.ResultState.Additional1);
                    return result;
                }

                messageOutput?.OutputInfo($"Update itemlist with new_item...");
                (from zeitPunktRel in objectsToCheck
                 join foundItem in dbConnector.Objects1 on new { zeitPunktRel.Name, zeitPunktRel.GUID_Parent } equals
                                                                  new { foundItem.Name, foundItem.GUID_Parent } into foundItems
                 from foundItem in foundItems.DefaultIfEmpty()
                 select new { zeitPunktRel, foundItem }).ToList().ForEach(zeitP =>
                 {
                     if (zeitP.foundItem == null)
                     {
                         zeitP.zeitPunktRel.New_Item = true;
                     }
                     else
                     {
                         zeitP.zeitPunktRel.GUID = zeitP.foundItem.GUID;
                         zeitP.zeitPunktRel.New_Item = false;
                     }

                 });

                messageOutput?.OutputInfo($"Updated itemlist with new_item.");

                var itemsToSave = objectsToCheck.Where(zeitP => zeitP.New_Item == true).GroupBy(zeitP => new { zeitP.Name, zeitP.GUID_Parent, zeitP.Type }).Select(grp => new clsOntologyItem { GUID = globals.NewGUID, Name = grp.Key.Name, GUID_Parent = grp.Key.GUID_Parent, Type = grp.Key.Type }).ToList();
                itemsToSave.ForEach(itemToSave =>
                {
                    itemToSave.GUID = globals.NewGUID;
                });

                if (itemsToSave.Any())
                {
                    messageOutput?.OutputInfo($"Save {itemsToSave.Count} items...");
                    result.ResultState = dbConnector.SaveObjects(itemsToSave);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while saving the items!";
                        messageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }
                    messageOutput?.OutputInfo($"Saved items.");
                }

                var searchDateTimeStamp = zeitpunktItemList.Select(zeitpunkt => new clsObjectAtt { ID_Object = zeitpunkt.GUID, ID_AttributeType = Zeitpunkt.Config.LocalData.AttributeType_DateTimestamp.GUID }).ToList();

                var dbReaderDateTimeStamp = new OntologyModDBConnector(globals);

                if (searchDateTimeStamp.Any())
                {
                    messageOutput?.OutputInfo($"Get DateTimeStamp attributes...");
                    result.ResultState = dbReaderDateTimeStamp.GetDataObjectAtt(searchDateTimeStamp);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while saving the items!";
                        messageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }
                    messageOutput?.OutputInfo($"Have DateTimeStamp attributes.");
                }

                var attributesToCreate = (from zeitpunkt in zeitpunktItemList.Where(ztp => ztp.Mark.Value)
                                          join attribute in dbReaderDateTimeStamp.ObjAtts on zeitpunkt.GUID equals attribute.ID_Object into attributes
                                          from attribute in attributes.DefaultIfEmpty()
                                          where attribute == null
                                          select relationConfig.Rel_ObjectAttribute(zeitpunkt, Zeitpunkt.Config.LocalData.AttributeType_DateTimestamp, zeitpunkt.Val_Date)).ToList();

                var attributesToDelete = (from zeitpunkt in zeitpunktItemList.Where(ztp => ztp.Mark.Value)
                                          join attribute in dbReaderDateTimeStamp.ObjAtts on zeitpunkt.GUID equals attribute.ID_Attribute into attributes
                                          from attribute in attributes.DefaultIfEmpty()
                                          where attribute != null && !attribute.Val_Date.Value.Equals(zeitpunkt.Val_Date.Value)
                                          select new clsObjectAtt { ID_Attribute = attribute.ID_Attribute, ID_Object = attribute.ID_Object }).ToList();
                attributesToCreate.AddRange((from zeitpunkt in zeitpunktItemList.Where(ztp => ztp.Mark.Value)
                                             join attributeToDelete in attributesToDelete on zeitpunkt.GUID equals attributeToDelete.ID_Object
                                             select zeitpunkt).GroupBy(zeitP => new { zeitP.GUID, zeitP.Name, zeitP.GUID_Parent, zeitP.Type, zeitP.Val_Date })
                                          .Select(grp => relationConfig.Rel_ObjectAttribute(new clsOntologyItem
                                          {
                                              GUID = grp.Key.GUID,
                                              Name = grp.Key.Name,
                                              GUID_Parent = grp.Key.GUID_Parent,
                                              Type = grp.Key.Type
                                          }, Zeitpunkt.Config.LocalData.AttributeType_DateTimestamp, grp.Key.Val_Date)));

                messageOutput?.OutputInfo($"{attributesToDelete.Count} Attributes to delete ...");

                if (attributesToDelete.Any())
                {
                    result.ResultState = dbConnector.DelObjectAtts(attributesToDelete);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while deleting the Attributes!";
                        messageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }
                }

                messageOutput?.OutputInfo($"Deleted Attributes .");

                messageOutput?.OutputInfo($"{attributesToCreate.Count} Attributes to create ...");
                if (attributesToCreate.Any())
                {
                    result.ResultState = dbConnector.SaveObjAtt(attributesToCreate);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while saving the DateTimeStamps!";
                        messageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }
                }
                messageOutput?.OutputInfo($"Created Attributes.");

                (from objectToCheck in objectsToCheck
                 join savedItem in itemsToSave on new { objectToCheck.Name, objectToCheck.GUID_Parent } equals
                                                  new { savedItem.Name, savedItem.GUID_Parent } into savedItems
                 from savedItem in savedItems.DefaultIfEmpty()
                 join zeitP in zeitpunktItemList on objectToCheck.GUID_Related equals zeitP.GUID
                 select new { objectToCheck, savedItem, zeitP }).ToList().ForEach(itm =>
                 {
                     if (itm.objectToCheck.New_Item.Value)
                     {
                         itm.objectToCheck.GUID = itm.savedItem.GUID;
                     }

                     relationsToCheck.Add(relationConfig.Rel_ObjectRelation(itm.zeitP, itm.objectToCheck, Config.LocalData.RelationType_belonging));
                 });

                var relationsToSave = new List<clsObjectRel>();
                var relationsToDelete = new List<clsObjectRel>();
                messageOutput?.OutputInfo($"Query relations...");
                if (relationsToCheck.Any())
                {

                    result.ResultState = dbConnector.GetDataObjectRel(zeitpunktItemList.Select(obj => new clsObjectRel { ID_Object = obj.GUID, ID_RelationType = Config.LocalData.RelationType_belonging.GUID }).ToList());
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the relations!";
                        messageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }

                    relationsToSave = (from relToCheck in relationsToCheck
                                       join relExist in dbConnector.ObjectRels on new { relToCheck.ID_Object, relToCheck.ID_RelationType, relToCheck.ID_Other } equals
                                                                                  new { relExist.ID_Object, relExist.ID_RelationType, relExist.ID_Other } into relsExist
                                       from relExist in relsExist.DefaultIfEmpty()
                                       where relExist == null
                                       select relToCheck).ToList();
                    relationsToDelete = (from relExist in dbConnector.ObjectRels
                                         join relToCheck in relationsToCheck on new { relExist.ID_Object, relExist.ID_RelationType, relExist.ID_Other } equals
                                                                                new { relToCheck.ID_Object, relToCheck.ID_RelationType, relToCheck.ID_Other } into relsToCheck2
                                         from relToCheck in relsToCheck2.DefaultIfEmpty()
                                         where relToCheck == null
                                         select relExist).ToList();
                }
                messageOutput?.OutputInfo($"Have {relationsToSave.Count} relations to be saved.");

                if (relationsToSave.Any())
                {
                    messageOutput?.OutputInfo($"Save relations...");
                    result.ResultState = dbConnector.SaveObjRel(relationsToSave);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while saving the relations!";
                        messageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }
                    messageOutput?.OutputInfo($"Saved relations.");
                }

                messageOutput?.OutputInfo($"Have {relationsToDelete.Count} relations to be deleted.");
                if (relationsToDelete.Any())
                {
                    messageOutput?.OutputInfo($"Delete relations...");
                    result.ResultState = dbConnector.DelObjectRels(relationsToDelete.Select(rel => new clsObjectRel { ID_Object = rel.ID_Object, ID_RelationType = rel.ID_RelationType, ID_Other = rel.ID_Other }).ToList());
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while deleting the relations!";
                        messageOutput?.OutputError(result.ResultState.Additional1);
                        return result;
                    }
                    messageOutput?.OutputInfo($"Delete relations.");
                }

                result.Result.SucceededCompletes.AddRange(itemsToSave);
                messageOutput?.OutputInfo($"Finished completion.");

                return result;
            });

            return taskResult;
        }

        public DateTimeZeitpunktController(Globals globals): base(globals)
        {

        }

    }
}
