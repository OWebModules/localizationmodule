﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocalizationModule.Models
{
    public class DateRangeModel
    {
        public List<clsObjectRel> ZeitraumListToReferences { get; set; } = new List<clsObjectRel>();
        public List<clsObjectAtt> DateRanges { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectAtt> DaysBack { get; set; } = new List<clsObjectAtt>();
        public List<clsObjectAtt> HoursBack { get; set; } = new List<clsObjectAtt>();

        public List<clsObjectAtt> MinutesBack { get; set; } = new List<clsObjectAtt>();
    }
}
