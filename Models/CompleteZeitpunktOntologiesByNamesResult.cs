﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocalizationModule.Models
{
    public class CompleteZeitpunktOntologiesByNamesResult
    {
        public List<clsOntologyItem> SucceededCompletes { get; set; } = new List<clsOntologyItem>();
    }
}
