﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocalizationModule.Models
{
    public class GetLocalizedGuiEntriesResult
    {
        public List<GuiEntry> GuiEntries { get; set; }
    }
}
