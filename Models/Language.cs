﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocalizationModule.Models
{
    public class Language
    {
        public string IdLanguage { get; set; }
        public string NameLanguage { get; set; }

        public string IdTwoLetterIso { get; set; }
        public string NameTwoLetterIso { get; set; }
    }
}
