﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocalizationModule.Models
{
    public class GetLocalizedGuiEntriesModelResult
    {
        public clsOntologyItem Reference { get; set; }
        public List<clsObjectRel> GuiEntriesToReferences { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> GuiEntriesToGuiEntries { get; set; } = new List<clsObjectRel>();
        public List<clsOntologyItem> GuiEntries { get; set; } = new List<clsOntologyItem>();
        public List<clsObjectRel> GuiEntriesToGuiCaptions { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> GuiCaptionsToLanguages { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> GuiEntriesToToolTipMessages { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> ToolTipMessagesToLanguages { get; set; } = new List<clsObjectRel>();
        public List<clsObjectRel> LanguagesToTwoLetterIsos { get; set; } = new List<clsObjectRel>();
    }
}
