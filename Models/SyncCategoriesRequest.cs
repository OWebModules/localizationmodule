﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LocalizationModule.Models
{
    public class SyncCategoriesRequest
    {
        public List<clsOntologyItem> SyncList { get; private set; }
        public CancellationToken CancellationToken { get; private set; }
        public IMessageOutput MessageOutput { get; set; }

        public SyncCategoriesRequest(List<clsOntologyItem> syncList, CancellationToken cancellation)
        {
            SyncList = syncList;
            CancellationToken = cancellation;
        }
    }
}
