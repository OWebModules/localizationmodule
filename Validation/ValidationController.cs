﻿using LocalizationModule.Models;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocalizationModule.Validation
{
    public static class ValidationController
    {
        public static clsOntologyItem ValidateCompleteZeitpunktOntologiesByNamesRequest(CompleteZeitpunktOntologiesByNamesRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (!string.IsNullOrEmpty(request.IdConfig) && !globals.is_GUID(request.IdConfig))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdConfig is no valid GUID!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateGetDateRangeRequest(GetDateRangeRequest request, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (!request.IdReferences.Any() || request.IdReferences.Any(idReference => string.IsNullOrEmpty(idReference) || request.IdReferences.Any(idRef => !globals.is_GUID(idRef))))
            {
                result = globals.LState_Error.Clone();
                result.Additional1 = "IdReference is no valid GUID!";
                return result;
            }

            return result;
        }

        public static clsOntologyItem ValidateAndSetDateRangeModel(DateRangeModel model, OntologyModDBConnector dbReader, string property, Globals globals)
        {
            var result = globals.LState_Success.Clone();

            if (property == nameof(DateRangeModel.ZeitraumListToReferences))
            {
                model.ZeitraumListToReferences = dbReader.ObjectRels;
            }
            else if (property == nameof(DateRangeModel.DateRanges))
            {
                model.DateRanges = dbReader.ObjAtts.Where(objAtt => objAtt.ID_AttributeType == DateRange.Config.LocalData.AttributeType_Daterange.GUID).ToList();
                var zeitraumCount = model.ZeitraumListToReferences.GroupBy(rel => rel.ID_Object).Count();
                if (model.DateRanges.Count * 2 > zeitraumCount)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You have {zeitraumCount} Zeitraum-elements and {model.DateRanges.Count} Dateranges! Allowed is max two Daterange-elements for one Zeitraum-element!";
                    return result;
                }
            }
            else if (property == nameof(DateRangeModel.DaysBack))
            {
                model.DaysBack = dbReader.ObjAtts.Where(objAtt => objAtt.ID_AttributeType == DateRange.Config.LocalData.AttributeType_Days_back.GUID).ToList();
                var zeitraumCount = model.ZeitraumListToReferences.GroupBy(rel => rel.ID_Object).Count();
                if (model.DaysBack.Count > zeitraumCount)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You have {zeitraumCount} Zeitraum-elements and {model.DaysBack.Count} Days Back-elements! Allowed is max one Days Back-element for one Zeitraum-element!";
                    return result;
                }
            }
            else if (property == nameof(DateRangeModel.HoursBack))
            {
                model.HoursBack = dbReader.ObjAtts.Where(objAtt => objAtt.ID_AttributeType == DateRange.Config.LocalData.AttributeType_Hours_back.GUID).ToList();
                var zeitraumCount = model.ZeitraumListToReferences.GroupBy(rel => rel.ID_Object).Count();
                if (model.HoursBack.Count > zeitraumCount)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You have {zeitraumCount} Zeitraum-elements and {model.HoursBack.Count} Hours Back-elements! Allowed is max one Hours Back-element for one Zeitraum-element!";
                    return result;
                }
            }
            else if (property == nameof(DateRangeModel.MinutesBack))
            {
                model.MinutesBack = dbReader.ObjAtts.Where(objAtt => objAtt.ID_AttributeType == DateRange.Config.LocalData.AttributeType_Minutes_back.GUID).ToList();
                var zeitraumCount = model.ZeitraumListToReferences.GroupBy(rel => rel.ID_Object).Count();
                if (model.MinutesBack.Count > zeitraumCount)
                {
                    result = globals.LState_Error.Clone();
                    result.Additional1 = $"You have {zeitraumCount} Zeitraum-elements and {model.MinutesBack.Count} Minutes Back-elements! Allowed is max one Minutes Back-element for one Zeitraum-element!";
                    return result;
                }
            }

            return result;
        }
    }
}
