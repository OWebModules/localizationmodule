﻿using LocalizationModule.Models;
using LocalizationModule.Services;
using LocalizationModule.Validation;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Interfaces;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LocalizationModule
{
    public class ZeitpunktController : IOItemNameProvider
    {
        public Globals Globals { get; set; }
        public async Task<ResultItem<List<ZeitpunktModel>>> GetZeitpunktModel(string idZeitpunkt = null)
        {
            var taskResult = await Task.Run<ResultItem<List<ZeitpunktModel>>>(async () =>
            {
                var result = new ResultItem<List<ZeitpunktModel>>
                {
                    ResultState = Globals.LState_Success.Clone(),
                    Result = new List<ZeitpunktModel>()
                };

                var elasticAgent = new ElasticAgent(Globals);

                var getZeitpunktModelResult = await elasticAgent.GetZeitpunktModel(idZeitpunkt);
                result.ResultState = getZeitpunktModelResult.ResultState;

                if (result.ResultState.GUID == Globals.LState_Error.GUID)
                {
                    return result;
                }

                getZeitpunktModelResult.Result.ForEach(zeitpunkt => zeitpunkt.CreateDatetimeZeitpunkt());

                result.Result = getZeitpunktModelResult.Result;

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<CompleteZeitpunktOntologiesByNamesResult>> CompleteZeitpunktOntologiesByNames(CompleteZeitpunktOntologiesByNamesRequest request)
        {
            var taskResult = await Task.Run<ResultItem<CompleteZeitpunktOntologiesByNamesResult>>(async () =>
           {
               var result = new ResultItem<CompleteZeitpunktOntologiesByNamesResult>
               {
                   ResultState = Globals.LState_Success.Clone(),
                   Result = new CompleteZeitpunktOntologiesByNamesResult()
               };

               request.MessageOutput?.OutputInfo("Validate request...");

               var validationResult = ValidationController.ValidateCompleteZeitpunktOntologiesByNamesRequest(request, Globals);

               result.ResultState = validationResult;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo("Validated request.");

               var elasticAgent = new ElasticAgent(Globals);


               request.MessageOutput?.OutputInfo("Get config...");
               var configResult = await elasticAgent.GetCompleteZeitpunktOntologiesByNameConfig(request);

               result.ResultState = configResult.ResultState;

               if (result.ResultState.GUID == Globals.LState_Error.GUID)
               {
                   request.MessageOutput?.OutputError(result.ResultState.Additional1);
                   return result;
               }

               request.MessageOutput?.OutputInfo("Have config.");

               List<clsOntologyItem> zeitpunkte = new List<clsOntologyItem>();
               if (!configResult.Result.AllZeitpunkte)
               {
                   zeitpunkte = configResult.Result.ConfigsToZeitpunkte.GroupBy(rel => new { GUID = rel.ID_Other, Name = rel.Name_Other, GUID_Parent = rel.ID_Parent_Other }).Select(grp => new clsOntologyItem
                   {
                       GUID = grp.Key.GUID,
                       Name = grp.Key.Name,
                       GUID_Parent = grp.Key.GUID_Parent,
                       Type = Globals.Type_Object
                   }).ToList();
               }
               else
               {
                   var searchZeitpunkte = new List<clsOntologyItem>
                   {
                       new clsOntologyItem
                       {
                           GUID_Parent = Zeitpunkt.Config.LocalData.Class_Zeitpunkt.GUID
                       }
                   };

                   var dbReaderZeitpunkte = new OntologyModDBConnector(Globals);

                   result.ResultState = dbReaderZeitpunkte.GetDataObjects(searchZeitpunkte);

                   if (result.ResultState.GUID == Globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Zeitpunkte!";
                       request.MessageOutput?.OutputError(result.ResultState.Additional1);
                       return result;
                   }

                   zeitpunkte = dbReaderZeitpunkte.Objects1;
               }

               var types = Assembly.GetExecutingAssembly().GetTypes().Where(type => type.BaseType == typeof(ZeitpunktControllerBase));

               var zeitpunktControllerList = new List<ZeitpunktControllerBase>();
               foreach (var zeitpunktControllerType in types)
               {
                   var zeitpunktController = (ZeitpunktControllerBase)Activator.CreateInstance(zeitpunktControllerType, Globals);
                   zeitpunktControllerList.Add(zeitpunktController);
               }

               var controllerMapList = new List<CompleteZeitpunktControllerZeitpunktListMap>();
               foreach (var zeitpunkt in zeitpunkte)
               {
                   var controller = zeitpunktControllerList.FirstOrDefault(contr => contr.IsResponsible(zeitpunkt.Name));
                   if (controller != null)
                   {
                       var controllerMap = controllerMapList.FirstOrDefault(map => map.Controller == controller);
                       if (controllerMap == null)
                       {
                           controllerMap = new CompleteZeitpunktControllerZeitpunktListMap
                           {
                               Controller = controller
                           };
                           controllerMapList.Add(controllerMap);
                       }

                       controllerMap.ZeitpunktListToComplete.Add(zeitpunkt);
                   }
               }
               foreach (var controllerMap in controllerMapList)
               {
                   var completeResult = await controllerMap.Controller.CompleteZeitpunkte(controllerMap.ZeitpunktListToComplete, request.MessageOutput);
                   result.ResultState = completeResult.ResultState;
                   result.Result.SucceededCompletes.AddRange(completeResult.Result.SucceededCompletes);
               }



               return result;
           });

            return taskResult;
        }

        public async Task<clsOntologyItem> SyncName(clsOntologyItem oItemToParse = null)
        {
            var taskResult = await Task.Run<clsOntologyItem>(async () =>
            {
                var result = Globals.LState_Success.Clone();

                var elasticAgent = new ElasticAgent(Globals);
                var zeitpunktItems = new List<clsOntologyItem>();

                if (oItemToParse == null)
                {
                    var readResult = await elasticAgent.GetAllZeitpunkte();
                    result = readResult.ResultState;
                    if (result.GUID == Globals.LState_Error.GUID)
                    {
                        return result;
                    }
                    zeitpunktItems = readResult.Result;
                }
                else
                {
                    zeitpunktItems.Add(oItemToParse);
                }

                foreach (var zeitpunktItem in zeitpunktItems)
                {
                    if (string.IsNullOrEmpty(zeitpunktItem.GUID))
                    {
                        zeitpunktItem.New_Item = true;
                    }
                    else
                    {
                        zeitpunktItem.New_Item = false;
                    }

                    ZeitpunktModel zeitPunktModel = null;
                    var objectsToSave = new List<clsOntologyItem>();

                    // Unique: Min == 1 && Max1 == 1
                    if (zeitpunktItem.New_Item.Value && !((zeitpunktItem.Min ?? 0) == 1 && (zeitpunktItem.Max1 ?? 0) == 1))
                    {
                        oItemToParse.GUID = Globals.NewGUID;
                        objectsToSave.Add(zeitpunktItem);
                    }
                    zeitPunktModel = new ZeitpunktModel(zeitpunktItem.Name);


                }

                return result;
            });



            return taskResult;
        }



        public List<string> GetIdsClass()
        {
            return new List<string> { Config.LocalData.Class_Zeitpunkt.GUID };
        }

        public ZeitpunktController(Globals globals)
        {
            Globals = globals;
        }
    }
}
