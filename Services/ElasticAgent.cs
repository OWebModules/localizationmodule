﻿using LocalizationModule.Models;
using LocalizationModule.Validation;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LocalizationModule.Services
{
    public class ElasticAgent
    {
        private Globals globals;

        public async Task<ResultItem<CompleteZeitpunktOntologiesByNameConfig>> GetCompleteZeitpunktOntologiesByNameConfig(CompleteZeitpunktOntologiesByNamesRequest request)
        {
            var taskResult = await Task.Run<ResultItem<CompleteZeitpunktOntologiesByNameConfig>>(() =>
           {
               var result = new ResultItem<CompleteZeitpunktOntologiesByNameConfig>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new CompleteZeitpunktOntologiesByNameConfig()
               };

               var dbReaderRootConfig = new OntologyModDBConnector(globals);

               var validateRequestResult = ValidationController.ValidateCompleteZeitpunktOntologiesByNamesRequest(request, globals);

               result.ResultState = validateRequestResult;

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   return result;
               }

               if (!string.IsNullOrEmpty(request.IdConfig))
               {
                   var searchRootConfig = new List<clsOntologyItem>
                   {
                       new clsOntologyItem
                       {
                           GUID = request.IdConfig,
                           GUID_Parent = Zeitpunkt.Config.LocalData.Class_Complete_Zeitpunkt_Ontologies.GUID
                       }
                   };

                   result.ResultState = dbReaderRootConfig.GetDataObjects(searchRootConfig);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Root-Config!";
                       return result;
                   }

                   result.Result.RootConfig = dbReaderRootConfig.Objects1.FirstOrDefault();

                   if (result.Result.RootConfig == null)
                   {
                       result.ResultState = globals.LState_Error.Clone();
                       result.ResultState.Additional1 = "IdConfig is not of correct class!";
                       return result;
                   }

                   var searchSubConfigs = new List<clsObjectRel>
                   {
                       new clsObjectRel
                       {
                           ID_Object = result.Result.RootConfig.GUID,
                           ID_RelationType = Zeitpunkt.Config.LocalData.ClassRel_Complete_Zeitpunkt_Ontologies_contains_Complete_Zeitpunkt_Ontologies.ID_RelationType,
                           ID_Parent_Other = Zeitpunkt.Config.LocalData.ClassRel_Complete_Zeitpunkt_Ontologies_contains_Complete_Zeitpunkt_Ontologies.ID_Class_Right
                       }
                   };

                   var dbReaderSubConfigs = new OntologyModDBConnector(globals);

                   result.ResultState = dbReaderSubConfigs.GetDataObjectRel(searchSubConfigs);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Subconfigs!";
                       return result;
                   }

                   result.Result.Configs = dbReaderSubConfigs.ObjectRels.Select(rel => new clsOntologyItem
                   {
                       GUID = rel.ID_Other,
                       Name = rel.Name_Other,
                       GUID_Parent = rel.ID_Parent_Other,
                       Type = rel.Ontology
                   }).ToList();

                   if (!result.Result.Configs.Any())
                   {
                       result.Result.Configs.Add(result.Result.RootConfig);
                   }


                   var searchZeitpunkte = result.Result.Configs.Select(config => new clsObjectRel
                   {
                       ID_Object = config.GUID,
                       ID_RelationType = Zeitpunkt.Config.LocalData.ClassRel_Complete_Zeitpunkt_Ontologies_contains_Zeitpunkt.ID_RelationType,
                       ID_Parent_Other = Zeitpunkt.Config.LocalData.ClassRel_Complete_Zeitpunkt_Ontologies_contains_Zeitpunkt.ID_Class_Right
                   }).ToList();

                   var dbReaderConfigsToZeitpunkte = new OntologyModDBConnector(globals);

                   result.ResultState = dbReaderConfigsToZeitpunkte.GetDataObjectRel(searchZeitpunkte);

                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the Zeitpunkte!";
                       return result;
                   }

                   result.Result.ConfigsToZeitpunkte = dbReaderConfigsToZeitpunkte.ObjectRels;
               }

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<List<clsOntologyItem>>> GetAllZeitpunkte()
        {
            var taskResult = await Task.Run<ResultItem<List<clsOntologyItem>>>(() =>
            {
                var result = new ResultItem<List<clsOntologyItem>>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new List<clsOntologyItem>()
                };

                var dbReader = new OntologyModDBConnector(globals);

                result.ResultState = dbReader.GetDataObjects(new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID_Parent = Config.LocalData.Class_Zeitpunkt.GUID
                    }
                });


                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while getting the Zeitpunkte!";
                    return result;
                }

                result.Result = dbReader.Objects1;

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> SaveTwoLetterIsos(List<clsOntologyItem> twoLetterIsosToSave, List<clsOntologyItem> languagesToSave, List<clsObjectRel> languagesToTwoLetters)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = globals.LState_Success.Clone();

                var dbWriter = new OntologyModDBConnector(globals);

                if (languagesToSave.Any())
                {
                    result = dbWriter.SaveObjects(languagesToSave);

                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving the languages!";
                        return result;
                    }
                }


                if (twoLetterIsosToSave.Any())
                {
                    result = dbWriter.SaveObjects(twoLetterIsosToSave);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving the Two Letter Iso Names!";
                        return result;
                    }
                }
                
                if (languagesToTwoLetters.Any())
                {
                    result = dbWriter.SaveObjRel(languagesToTwoLetters);
                    if (result.GUID == globals.LState_Error.GUID)
                    {
                        result.Additional1 = "Error while saving the relation between languages and Two Letter Iso Names!";
                        return result;
                    }
                }

                return result;
            });

            return taskResult;
        }
        public async Task<ResultItem<ImportCulturesModel>> GetImportCultureModel()
        {
            var taskResult = await Task.Run<ResultItem<ImportCulturesModel>>(() =>
           {
               var result = new ResultItem<ImportCulturesModel>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new ImportCulturesModel()
               };

               var searchTwoLetterISOs = new List<clsOntologyItem>
               {
                   new clsOntologyItem
                   {
                       GUID_Parent = Config.LocalData.Class_TwoLetterISOLanguageName.GUID
                   }
               };

               var dbReaderTwoLetterIsos = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderTwoLetterIsos.GetDataObjects(searchTwoLetterISOs);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the TwoLetterIsos";
                   return result;
               }

               result.Result.TwoLetterIsoLanguages = dbReaderTwoLetterIsos.Objects1;

               var searchTwoLetterIsosToLanguages = new List<clsObjectRel>
               {
                   new clsObjectRel
                   {
                       ID_Parent_Object = Config.LocalData.ClassRel_Language_is_of_Type_TwoLetterISOLanguageName.ID_Class_Left,
                       ID_RelationType = Config.LocalData.ClassRel_Language_is_of_Type_TwoLetterISOLanguageName.ID_RelationType,
                       ID_Parent_Other = Config.LocalData.ClassRel_Language_is_of_Type_TwoLetterISOLanguageName.ID_Class_Right
                   }
               };

               var dbReaderTwoLetterIsosToLanguages = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderTwoLetterIsosToLanguages.GetDataObjectRel(searchTwoLetterIsosToLanguages);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the relation between the languages and the TwoLetterIsos";
                   return result;
               }

               result.Result.TwoLetterIsoLanguagesToLanguage = dbReaderTwoLetterIsosToLanguages.ObjectRels;

               var searchLanguages = new List<clsOntologyItem>
               {
                   new clsOntologyItem
                   {
                       GUID_Parent = Config.LocalData.Class_Language.GUID
                   }
               };

               var dbReaderLanguages = new OntologyModDBConnector(globals);

               result.ResultState = dbReaderLanguages.GetDataObjects(searchLanguages);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting the languages!";
                   return result;
               }

               result.Result.Languages = dbReaderLanguages.Objects1;

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<GetLocalizedGuiEntriesModelResult>> GetLocalizationEntriesModel(GetLocalizedGuiEntriesRequest request)
        {
            var taskResult = await Task.Run<ResultItem<GetLocalizedGuiEntriesModelResult>>(() =>
            {
                var result = new ResultItem<GetLocalizedGuiEntriesModelResult>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new GetLocalizedGuiEntriesModelResult()
                };

                if (string.IsNullOrEmpty(request.IdReference) || !globals.is_GUID(request.IdReference))
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "Request ist no valid Id";
                    return result;
                }

                var searchReference = new List<clsOntologyItem>
                {
                    new clsOntologyItem
                    {
                        GUID = request.IdReference
                    }
                };

                var dbReaderReference = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderReference.GetDataObjects(searchReference);

                result.Result.Reference = dbReaderReference.Objects1.FirstOrDefault();

                if (result.Result.Reference == null)
                {
                    result.ResultState = globals.LState_Error.Clone();
                    result.ResultState.Additional1 = "No valid reference provided!";
                    return result;
                }

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while loading Reference";
                    return result;
                }

                var searchGuiEntries = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Other = request.IdReference,
                        ID_RelationType = Config.LocalData.RelationType_belongs_to.GUID,
                        ID_Parent_Object = Config.LocalData.Class_GUI_Entires.GUID
                    }
                };

                var dbReaderGuiEntries = new OntologyModDBConnector(globals);

                result.ResultState = dbReaderGuiEntries.GetDataObjectRel(searchGuiEntries);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    result.ResultState.Additional1 = "Error while loading Gui-Entries of references";
                    return result;
                }

                result.Result.GuiEntriesToReferences = dbReaderGuiEntries.ObjectRels;

                var searchGuiSubEntries = result.Result.GuiEntriesToReferences.Select(rel => new clsObjectRel
                {
                    ID_Object = rel.ID_Object,
                    ID_RelationType = Config.LocalData.ClassRel_GUI_Entires_contains_GUI_Entires.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_GUI_Entires_contains_GUI_Entires.ID_Class_Right
                }).ToList();

                if (searchGuiSubEntries.Any())
                {
                    var dbReaderSubEntries = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderGuiEntries.GetDataObjectRel(searchGuiSubEntries);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while loading Sub Gui-Entries";
                        return result;
                    }

                    result.Result.GuiEntriesToGuiEntries = dbReaderGuiEntries.ObjectRels;
                }

                result.Result.GuiEntries = result.Result.GuiEntriesToReferences.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Object,
                    Name = rel.Name_Object,
                    GUID_Parent = rel.ID_Parent_Object,
                    Type = globals.Type_Object
                }).ToList();

                result.Result.GuiEntries.AddRange(result.Result.GuiEntriesToGuiEntries.Select(rel => new clsOntologyItem
                {
                    GUID = rel.ID_Other,
                    Name = rel.Name_Other,
                    GUID_Parent = rel.ID_Parent_Other,
                    Type = globals.Type_Object
                }));

                var searchGuiEntriesToGuiCaptions = result.Result.GuiEntries.Select(rel => new clsObjectRel
                {
                    ID_Object = rel.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_GUI_Entires_is_defined_by_GUI_Caption.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_GUI_Entires_is_defined_by_GUI_Caption.ID_Class_Right
                }).ToList();

                if (searchGuiEntriesToGuiCaptions.Any())
                {
                    var dbReaderGuiEntriesToGuiCaptions = new OntologyModDBConnector(globals);
                    result.ResultState = dbReaderGuiEntriesToGuiCaptions.GetDataObjectRel(searchGuiEntriesToGuiCaptions);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while loading Gui-Captions.";
                        return result;
                    }

                    result.Result.GuiEntriesToGuiCaptions = dbReaderGuiEntriesToGuiCaptions.ObjectRels;
                }

                var searchGuiCaptionsToLanguages = result.Result.GuiEntriesToGuiCaptions.Select(rel => new clsObjectRel
                {
                    ID_Object = rel.ID_Other,
                    ID_RelationType = Config.LocalData.ClassRel_GUI_Caption_is_written_in_Language.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_GUI_Caption_is_written_in_Language.ID_Class_Right
                }).ToList();

                if (searchGuiCaptionsToLanguages.Any())
                {
                    var dbReaderGuiCaptionsToLanguages = new OntologyModDBConnector(globals);
                    result.ResultState = dbReaderGuiCaptionsToLanguages.GetDataObjectRel(searchGuiCaptionsToLanguages);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while loading Languages of Gui-Captions.";
                        return result;
                    }

                    result.Result.GuiCaptionsToLanguages = dbReaderGuiCaptionsToLanguages.ObjectRels;
                }

                var searchGuiEntriesToTooltipMessages = result.Result.GuiEntries.Select(guiEntry => new clsObjectRel
                {
                    ID_Object = guiEntry.GUID,
                    ID_RelationType = Config.LocalData.ClassRel_GUI_Entires_is_defined_by_ToolTip_Messages.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_GUI_Entires_is_defined_by_ToolTip_Messages.ID_Class_Right
                }).ToList();

                if (searchGuiEntriesToTooltipMessages.Any())
                {
                    var dbReaderGuiEntriesToTooltipMessages = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderGuiEntriesToTooltipMessages.GetDataObjectRel(searchGuiEntriesToTooltipMessages);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while loading Tooltip-Messages.";
                        return result;
                    }

                    result.Result.GuiEntriesToToolTipMessages = dbReaderGuiEntriesToTooltipMessages.ObjectRels;
                }

                var searchToolTipmessagesToLanguages = result.Result.GuiEntriesToToolTipMessages.Select(rel => new clsObjectRel
                {
                    ID_Object = rel.ID_Other,
                    ID_RelationType = Config.LocalData.ClassRel_ToolTip_Messages_is_written_in_Language.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_ToolTip_Messages_is_written_in_Language.ID_Class_Right
                }).ToList();

                if (searchToolTipmessagesToLanguages.Any())
                {
                    var dbReaderTooltipMessagesToLanguages = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderTooltipMessagesToLanguages.GetDataObjectRel(searchToolTipmessagesToLanguages);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while loading Languages of Tooltip-Messages.";
                        return result;
                    }

                    result.Result.ToolTipMessagesToLanguages = dbReaderTooltipMessagesToLanguages.ObjectRels;

                }

                var searchLanguagesToTwoLetterIso = result.Result.GuiCaptionsToLanguages.Select(rel => new clsObjectRel
                {
                    ID_Object = rel.ID_Other,
                    ID_RelationType = Config.LocalData.ClassRel_Language_is_of_Type_TwoLetterISOLanguageName.ID_RelationType,
                    ID_Parent_Other = Config.LocalData.ClassRel_Language_is_of_Type_TwoLetterISOLanguageName.ID_Class_Right
                }).ToList();

                if (searchLanguagesToTwoLetterIso.Any())
                {
                    var dbReaderLanguagesToTwoLetterIso = new OntologyModDBConnector(globals);

                    result.ResultState = dbReaderLanguagesToTwoLetterIso.GetDataObjectRel(searchLanguagesToTwoLetterIso);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while loading Two Letter Iso of Lanaguages.";
                        return result;
                    }

                    result.Result.LanguagesToTwoLetterIsos = dbReaderLanguagesToTwoLetterIso.ObjectRels;
                }

                return result;
            });

            return taskResult;
        }

        public async Task<ResultItem<List<ZeitpunktModel>>> GetZeitpunktModel(string idZeitpunkt = null)
        {
            var taskResult = await Task.Run<ResultItem<List<ZeitpunktModel>>>(() =>
           {
               var result = new ResultItem<List<ZeitpunktModel>>
               {
                   ResultState = globals.LState_Success.Clone(),
                   Result = new List<ZeitpunktModel>()
               };

               var dbReaderZeipunkt = new OntologyModDBConnector(globals);

               var searchZeitpunkt = new List<clsOntologyItem>
               {
                   new clsOntologyItem
                   {
                       GUID = idZeitpunkt,
                       GUID_Parent = Config.LocalData.Class_Zeitpunkt.GUID
                   }
               };

               result.ResultState = dbReaderZeipunkt.GetDataObjects(searchZeitpunkt);

               if (result.ResultState.GUID == globals.LState_Error.GUID)
               {
                   result.ResultState.Additional1 = "Error while getting Zeitpunkt";
                   return result;
               }

               var searchRels = dbReaderZeipunkt.Objects1.Select(obj => new clsObjectRel
               {
                   ID_Object = obj.GUID,
                   ID_RelationType = Config.LocalData.ClassRel_Zeitpunkt_belonging_Century.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Zeitpunkt_belonging_Century.ID_Class_Right
               }).ToList();

               searchRels.AddRange(dbReaderZeipunkt.Objects1.Select(obj => new clsObjectRel
               {
                   ID_Object = obj.GUID,
                   ID_RelationType = Config.LocalData.ClassRel_Zeitpunkt_belonging_Day.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Zeitpunkt_belonging_Day.ID_Class_Right
               }));

               searchRels.AddRange(dbReaderZeipunkt.Objects1.Select(obj => new clsObjectRel
               {
                   ID_Object = obj.GUID,
                   ID_RelationType = Config.LocalData.ClassRel_Zeitpunkt_belonging_Hour.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Zeitpunkt_belonging_Hour.ID_Class_Right
               }));

               searchRels.AddRange(dbReaderZeipunkt.Objects1.Select(obj => new clsObjectRel
               {
                   ID_Object = obj.GUID,
                   ID_RelationType = Config.LocalData.ClassRel_Zeitpunkt_belonging_Microsecond.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Zeitpunkt_belonging_Microsecond.ID_Class_Right
               }));

               searchRels.AddRange(dbReaderZeipunkt.Objects1.Select(obj => new clsObjectRel
               {
                   ID_Object = obj.GUID,
                   ID_RelationType = Config.LocalData.ClassRel_Zeitpunkt_belonging_Millisecond.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Zeitpunkt_belonging_Millisecond.ID_Class_Right
               }));

               searchRels.AddRange(dbReaderZeipunkt.Objects1.Select(obj => new clsObjectRel
               {
                   ID_Object = obj.GUID,
                   ID_RelationType = Config.LocalData.ClassRel_Zeitpunkt_belonging_Minute.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Zeitpunkt_belonging_Minute.ID_Class_Right
               }));

               searchRels.AddRange(dbReaderZeipunkt.Objects1.Select(obj => new clsObjectRel
               {
                   ID_Object = obj.GUID,
                   ID_RelationType = Config.LocalData.ClassRel_Zeitpunkt_belonging_Month.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Zeitpunkt_belonging_Month.ID_Class_Right
               }));

               searchRels.AddRange(dbReaderZeipunkt.Objects1.Select(obj => new clsObjectRel
               {
                   ID_Object = obj.GUID,
                   ID_RelationType = Config.LocalData.ClassRel_Zeitpunkt_belonging_Nanosecond.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Zeitpunkt_belonging_Nanosecond.ID_Class_Right
               }));

               searchRels.AddRange(dbReaderZeipunkt.Objects1.Select(obj => new clsObjectRel
               {
                   ID_Object = obj.GUID,
                   ID_RelationType = Config.LocalData.ClassRel_Zeitpunkt_belonging_Picosecond.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Zeitpunkt_belonging_Picosecond.ID_Class_Right
               }));

               searchRels.AddRange(dbReaderZeipunkt.Objects1.Select(obj => new clsObjectRel
               {
                   ID_Object = obj.GUID,
                   ID_RelationType = Config.LocalData.ClassRel_Zeitpunkt_belonging_Second.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Zeitpunkt_belonging_Second.ID_Class_Right
               }));

               searchRels.AddRange(dbReaderZeipunkt.Objects1.Select(obj => new clsObjectRel
               {
                   ID_Object = obj.GUID,
                   ID_RelationType = Config.LocalData.ClassRel_Zeitpunkt_belonging_Week.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Zeitpunkt_belonging_Week.ID_Class_Right
               }));

               searchRels.AddRange(dbReaderZeipunkt.Objects1.Select(obj => new clsObjectRel
               {
                   ID_Object = obj.GUID,
                   ID_RelationType = Config.LocalData.ClassRel_Zeitpunkt_belonging_Weekdays.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Zeitpunkt_belonging_Weekdays.ID_Class_Right
               }));

               searchRels.AddRange(dbReaderZeipunkt.Objects1.Select(obj => new clsObjectRel
               {
                   ID_Object = obj.GUID,
                   ID_RelationType = Config.LocalData.ClassRel_Zeitpunkt_belonging_Year.ID_RelationType,
                   ID_Parent_Other = Config.LocalData.ClassRel_Zeitpunkt_belonging_Year.ID_Class_Right
               }));

               var dbReaderRelations = new OntologyModDBConnector(globals);

               if (searchRels.Any())
               {
                   result.ResultState = dbReaderRelations.GetDataObjectRel(searchRels);
                   if (result.ResultState.GUID == globals.LState_Error.GUID)
                   {
                       result.ResultState.Additional1 = "Error while getting the relations";
                   }

                   result.Result = (from zeitpunkt in dbReaderZeipunkt.Objects1
                                    join relToCentury in dbReaderRelations.ObjectRels.Where(rel => rel.ID_Parent_Other == Config.LocalData.Class_Century.GUID) on zeitpunkt.GUID equals relToCentury.ID_Object into relToCenturies
                                    from relToCentury in relToCenturies.DefaultIfEmpty()
                                    join relToDay in dbReaderRelations.ObjectRels.Where(rel => rel.ID_Parent_Other == Config.LocalData.Class_Day.GUID) on zeitpunkt.GUID equals relToDay.ID_Object into relToDays
                                    from relToDay in relToDays.DefaultIfEmpty()
                                    join relToHour in dbReaderRelations.ObjectRels.Where(rel => rel.ID_Parent_Other == Config.LocalData.Class_Hour.GUID) on zeitpunkt.GUID equals relToHour.ID_Object into relToHours
                                    from relToHour in relToHours.DefaultIfEmpty()
                                    join relToMicrosecond in dbReaderRelations.ObjectRels.Where(rel => rel.ID_Parent_Other == Config.LocalData.Class_Microsecond.GUID) on zeitpunkt.GUID equals relToMicrosecond.ID_Object into relToMicroseconds
                                    from relToMicrosecond in relToMicroseconds.DefaultIfEmpty()
                                    join relToMillisecond in dbReaderRelations.ObjectRels.Where(rel => rel.ID_Parent_Other == Config.LocalData.Class_Millisecond.GUID) on zeitpunkt.GUID equals relToMillisecond.ID_Object into relToMilliseconds
                                    from relToMillisecond in relToMilliseconds.DefaultIfEmpty()
                                    join relToMinute in dbReaderRelations.ObjectRels.Where(rel => rel.ID_Parent_Other == Config.LocalData.Class_Minute.GUID) on zeitpunkt.GUID equals relToMinute.ID_Object into relToMinutes
                                    from relToMinute in relToMinutes.DefaultIfEmpty()
                                    join relToMonth in dbReaderRelations.ObjectRels.Where(rel => rel.ID_Parent_Other == Config.LocalData.Class_Month.GUID) on zeitpunkt.GUID equals relToMonth.ID_Object into relToMonths
                                    from relToMonth in relToMonths.DefaultIfEmpty()
                                    join relToNanosecond in dbReaderRelations.ObjectRels.Where(rel => rel.ID_Parent_Other == Config.LocalData.Class_Nanosecond.GUID) on zeitpunkt.GUID equals relToNanosecond.ID_Object into relToNanoseconds
                                    from relToNanosecond in relToNanoseconds.DefaultIfEmpty()
                                    join relToPicosecond in dbReaderRelations.ObjectRels.Where(rel => rel.ID_Parent_Other == Config.LocalData.Class_Picosecond.GUID) on zeitpunkt.GUID equals relToPicosecond.ID_Object into relToPicoseconds
                                    from relToPicosecond in relToPicoseconds.DefaultIfEmpty()
                                    join relToSecond in dbReaderRelations.ObjectRels.Where(rel => rel.ID_Parent_Other == Config.LocalData.Class_Second.GUID) on zeitpunkt.GUID equals relToSecond.ID_Object into relToSeconds
                                    from relToSecond in relToSeconds.DefaultIfEmpty()
                                    join relToWeek in dbReaderRelations.ObjectRels.Where(rel => rel.ID_Parent_Other == Config.LocalData.Class_Week.GUID) on zeitpunkt.GUID equals relToWeek.ID_Object into relToWeeks
                                    from relToWeek in relToWeeks.DefaultIfEmpty()
                                    join relToWeekday in dbReaderRelations.ObjectRels.Where(rel => rel.ID_Parent_Other == Config.LocalData.Class_Weekdays.GUID) on zeitpunkt.GUID equals relToWeekday.ID_Object into relToWeekdays
                                    from relToWeekday in relToWeekdays.DefaultIfEmpty()
                                    join relToYear in dbReaderRelations.ObjectRels.Where(rel => rel.ID_Parent_Other == Config.LocalData.Class_Year.GUID) on zeitpunkt.GUID equals relToYear.ID_Object into relToYears
                                    from relToYear in relToYears.DefaultIfEmpty()
                                    select new ZeitpunktModel
                                    {
                                        Zeitpunkt = zeitpunkt,
                                        Century = relToCentury != null ? new clsOntologyItem
                                        {
                                            GUID = relToCentury.ID_Other,
                                            Name = relToCentury.Name_Other,
                                            GUID_Parent = relToCentury.ID_Parent_Other,
                                            Type = relToCentury.Ontology
                                        } : null,
                                        Day = relToDay != null ? new clsOntologyItem
                                        {
                                            GUID = relToDay.ID_Other,
                                            Name = relToDay.Name_Other,
                                            GUID_Parent = relToDay.ID_Parent_Other,
                                            Type = relToDay.Ontology
                                        } : null,
                                        Hour = relToHour != null ? new clsOntologyItem
                                        {
                                            GUID = relToHour.ID_Other,
                                            Name = relToHour.Name_Other,
                                            GUID_Parent = relToHour.ID_Parent_Other,
                                            Type = relToHour.Ontology
                                        } : null,
                                        Microsecond = relToMicrosecond != null ? new clsOntologyItem
                                        {
                                            GUID = relToMicrosecond.ID_Other,
                                            Name = relToMicrosecond.Name_Other,
                                            GUID_Parent = relToMicrosecond.ID_Parent_Other,
                                            Type = relToMicrosecond.Ontology
                                        } : null,
                                        Millisecond = relToMillisecond != null ? new clsOntologyItem
                                        {
                                            GUID = relToMillisecond.ID_Other,
                                            Name = relToMillisecond.Name_Other,
                                            GUID_Parent = relToMillisecond.ID_Parent_Other,
                                            Type = relToMillisecond.Ontology
                                        } : null,
                                        Minute = relToMinute != null ? new clsOntologyItem
                                        {
                                            GUID = relToMinute.ID_Other,
                                            Name = relToMinute.Name_Other,
                                            GUID_Parent = relToMinute.ID_Parent_Other,
                                            Type = relToMinute.Ontology
                                        } : null,
                                        Month = relToMonth != null ? new clsOntologyItem
                                        {
                                            GUID = relToMonth.ID_Other,
                                            Name = relToMonth.Name_Other,
                                            GUID_Parent = relToMonth.ID_Parent_Other,
                                            Type = relToMonth.Ontology
                                        } : null,
                                        Nanosecond = relToNanosecond != null ? new clsOntologyItem
                                        {
                                            GUID = relToNanosecond.ID_Other,
                                            Name = relToNanosecond.Name_Other,
                                            GUID_Parent = relToNanosecond.ID_Parent_Other,
                                            Type = relToNanosecond.Ontology
                                        } : null,
                                        Picosecond = relToPicosecond != null ? new clsOntologyItem
                                        {
                                            GUID = relToPicosecond.ID_Other,
                                            Name = relToPicosecond.Name_Other,
                                            GUID_Parent = relToPicosecond.ID_Parent_Other,
                                            Type = relToPicosecond.Ontology
                                        } : null,
                                        Second = relToSecond != null ? new clsOntologyItem
                                        {
                                            GUID = relToSecond.ID_Other,
                                            Name = relToSecond.Name_Other,
                                            GUID_Parent = relToSecond.ID_Parent_Other,
                                            Type = relToSecond.Ontology
                                        } : null,
                                        Week = relToWeek != null ? new clsOntologyItem
                                        {
                                            GUID = relToWeek.ID_Other,
                                            Name = relToWeek.Name_Other,
                                            GUID_Parent = relToWeek.ID_Parent_Other,
                                            Type = relToWeek.Ontology
                                        } : null,
                                        Weekdays = relToWeekday != null ? new clsOntologyItem
                                        {
                                            GUID = relToWeekday.ID_Other,
                                            Name = relToWeekday.Name_Other,
                                            GUID_Parent = relToWeekday.ID_Parent_Other,
                                            Type = relToWeekday.Ontology
                                        } : null,
                                        Year = relToYear != null ? new clsOntologyItem
                                        {
                                            GUID = relToYear.ID_Other,
                                            Name = relToYear.Name_Other,
                                            GUID_Parent = relToYear.ID_Parent_Other,
                                            Type = relToYear.Ontology
                                        } : null
                                    }).ToList();
               }

               return result;
           });

            return taskResult;
        }

        public async Task<ResultItem<DateRangeModel>> GetDateRangeModel(GetDateRangeRequest request)
        {
            var taskResult = await Task.Run(() =>
            {
                var result = new ResultItem<DateRangeModel>
                {
                    ResultState = globals.LState_Success.Clone(),
                    Result = new DateRangeModel()
                };

                var searchZeitraumList = request.IdReferences.Select(idRef => new clsObjectRel
                {
                    
                    ID_Parent_Object = DateRange.Config.LocalData.Class_Zeitraum.GUID,
                    ID_RelationType = DateRange.Config.LocalData.RelationType_belongs_to.GUID,
                    ID_Other = idRef
                    
                }).ToList();

                var dbReaderZeitraumList = new OntologyModDBConnector(globals);

                if (searchZeitraumList.Any())
                {
                    result.ResultState = dbReaderZeitraumList.GetDataObjectRel(searchZeitraumList);

                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Zeitraum-List!";
                        return result;
                    }
                }

                result.ResultState = ValidationController.ValidateAndSetDateRangeModel(result.Result, dbReaderZeitraumList, nameof(DateRangeModel.ZeitraumListToReferences), globals);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                var searchAttributes = new List<clsObjectAtt>();
                foreach (var zeitraumToRef in result.Result.ZeitraumListToReferences)
                {
                    searchAttributes.Add(new clsObjectAtt
                    {
                        ID_Object = zeitraumToRef.ID_Object,
                        ID_AttributeType = DateRange.Config.LocalData.AttributeType_Daterange.GUID
                    });

                    searchAttributes.Add(new clsObjectAtt
                    {
                        ID_Object = zeitraumToRef.ID_Object,
                        ID_AttributeType = DateRange.Config.LocalData.AttributeType_Days_back.GUID
                    });

                    searchAttributes.Add(new clsObjectAtt
                    {
                        ID_Object = zeitraumToRef.ID_Object,
                        ID_AttributeType = DateRange.Config.LocalData.AttributeType_Hours_back.GUID
                    });

                    searchAttributes.Add(new clsObjectAtt
                    {
                        ID_Object = zeitraumToRef.ID_Object,
                        ID_AttributeType = DateRange.Config.LocalData.AttributeType_Minutes_back.GUID
                    });
                }

                var dbReaderAttributes = new OntologyModDBConnector(globals);

                if (searchAttributes.Any())
                {
                    result.ResultState = dbReaderAttributes.GetDataObjectAtt(searchAttributes);
                    if (result.ResultState.GUID == globals.LState_Error.GUID)
                    {
                        result.ResultState.Additional1 = "Error while getting the Attributes of the Zeitraum-List!";
                        return result;
                    }
                }

                result.ResultState = ValidationController.ValidateAndSetDateRangeModel(result.Result, dbReaderAttributes, nameof(DateRangeModel.DateRanges), globals);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetDateRangeModel(result.Result, dbReaderAttributes, nameof(DateRangeModel.DaysBack), globals);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetDateRangeModel(result.Result, dbReaderAttributes, nameof(DateRangeModel.HoursBack), globals);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                result.ResultState = ValidationController.ValidateAndSetDateRangeModel(result.Result, dbReaderAttributes, nameof(DateRangeModel.MinutesBack), globals);

                if (result.ResultState.GUID == globals.LState_Error.GUID)
                {
                    return result;
                }

                return result;
            });

            return taskResult;
        }

        public ElasticAgent(Globals globals)
        {
            this.globals = globals;
        }
    }
}
